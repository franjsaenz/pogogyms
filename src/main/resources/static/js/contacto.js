$(function () {
    $('#contactoform').on('submit', function (evt) {
        evt.preventDefault();
        contact();
    });
});

function contact() {
    var payload = {
        userId: $('#userId').val(),
        email: $('#contactomail').val(),
        subject: $('#contactoasunto').val(),
        message: $('#contactotexto').val()
    };
    var post = {
        url: '/api/contact',
        body: JSON.stringify(payload),
        type: 'POST',
        suc: contactOk,
        msg: contactErr
    }
    sendToServer(post);
}

function contactOk(data) {
    $('#contactook').show();
}

function contactErr(msg) {
    $('#contactoerr').show();
    $('#contactoerrmsg').text(msg)
}

