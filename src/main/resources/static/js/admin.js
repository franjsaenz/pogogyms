$(function () {
    $('#nestmigrationreport').on('submit', function (evt) {
        evt.preventDefault();
        migrationReport();
    });
    $('#newnestreport').on('submit', function (evt) {
        evt.preventDefault();
        newNest();
    });
});

function migrationReport() {
    var report = {
        userId: $('#userId').val(),
        hour: parseInt($('#migrationhour').val()),
        minutes: parseInt($('#migrationmin').val())
    };
    var post = {
        url: '/api/nest/migration',
        body: JSON.stringify(report),
        type: 'POST',
        suc: migrationOk,
        msg: succMigration
    }
    sendToServer(post);
}

function migrationOk(data) {
    location.reload();
}

function succMigration(msg) {
    $('#migerrmsg').text(msg);
    $('#migerr').show();
}

function newNest() {
    var report = {
        userId: $('#userId').val(),
        geojson: $('#newnestgeojson').val(),
        name: $('#newnastname').val()
    };
    var post = {
        url: '/api/polygons',
        body: JSON.stringify(report),
        type: 'POST',
        suc: exOk,
        msg: exMigration
    }
    sendToServer(post);
}

function exOk(data) {
    location.reload();
}

function exMigration(msg) {
    $('#newnesterrmsg').text(msg);
    $('#newnesterr').show();
}

function banUser(userId) {
    var post = {
        url: '/api/user/' + userId,
        type: 'DELETE',
        suc: banOk,
        msg: banErr
    }
    sendToServer(post);
}

function banOk() {
    $('#btn-ban-user').text("Usuario baneado");
    $('#btn-ban-user').prop('disabled', true);
}

function banErr(msg) {
    $('#btn-ban-user').text(msg);
    $('#btn-ban-user').prop('disabled', true);
}

function notifyGymEx(userId, gymId, exCapable) {
    var report = {
        userId: userId,
        gymId: gymId,
        exCapable: exCapable
    }
    var post = {
        url: '/api/gym/ex',
        body: JSON.stringify(report),
        type: 'POST',
        suc: exGymOk,
        msg: exGymMsg
    }
    $('#gym-ex-on').di
    sendToServer(post);
}

function exGymOk() {
    $('#btn-gym-ex').prop('disable', true);
    $('#btn-gym-ex').text('Notificado')
}

function exGymMsg(msg) {
    $('#btn-gym-ex').prop('disable', true);
    $('#btn-gym-ex').text(msg)
}
