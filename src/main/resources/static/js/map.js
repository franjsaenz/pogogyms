var map;
var markers = [];
var exmarkers = [];
var nestMarkers = [];
var nestMarkersIcons = [];
var pkStops = [];
var s2Cells = [];

var filters = {
    gyms: 'false',
    exgyms: localStorage.getItem('pogogyms_filter_exgyms') == null ? 'true' : localStorage.getItem('pogogyms_filter_exgyms'),
    markers: localStorage.getItem('pogogyms_filter_markers') == null ? 'true' : localStorage.getItem('pogogyms_filter_markers'),
    pokestops: 'false',
    polygons: localStorage.getItem('pogogyms_filter_polygons') == null ? 'false' : localStorage.getItem('pogogyms_filter_polygons'),
    s2cells: 'false'
};

var mapLocs = {
    lat: localStorage.getItem('pogogyms_map_lat') == null ? -32.8928014 : localStorage.getItem('pogogyms_map_lat'),
    lng: localStorage.getItem('pogogyms_map_lng') == null ? -68.8445642 : localStorage.getItem('pogogyms_map_lng'),
    zoom: localStorage.getItem('pogogyms_map_zoom') == null ? 14 : localStorage.getItem('pogogyms_map_zoom')
};

$(function () {
    $('#reportnest').on('submit', function (evt) {
        evt.preventDefault();
        sendReport();
    });
    $('#reportraid').on('submit', function (evt) {
        evt.preventDefault();
        sendRaid();
    });
    startFilters();
    google.maps.event.addDomListener(window, 'load', initMap);
});

function initMap() {
    var z = parseInt(mapLocs.zoom);

    var location = new google.maps.LatLng(mapLocs.lat, mapLocs.lng);

    var mapCanvas = document.getElementById('map');
    var mapOptions = {
        center: location,
        clickableIcons: false,
        zoom: 14,
        mapTypeControl: false,
        streetViewControl: false,
        styles: gMapStyle,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        }
    }
    map = new google.maps.Map(mapCanvas, mapOptions);

    try {
        map.setZoom(z);
    } catch (e) {
        map.setZoom(14);
    }
    map.addListener('idle', function () {
        changeMap();
    });

    listGyms();
    listNests();
    try {
        listS2cells();
    } catch (e) {}
    listPokestops();
    getNestPokemons();
    endOfLifePopup();
}

function endOfLifePopup() {
    // var display = localStorage.getItem('pogogyms_end_of_life');
    // if (display == null) {
    //     $('#endoflife').modal('show');
    // }
    $('#endoflife').modal('show');
}

function hideEndOfLifePopup() {
    $('#endoflife').modal('hide');
    // localStorage.setItem('pogogyms_end_of_life', 'displayed');
}

function changeMap() {
    mapLocs.lat = map.getCenter().lat();
    mapLocs.lng = map.getCenter().lng();
    mapLocs.zoom = map.getZoom();
    localStorage.setItem('pogogyms_map_lat', mapLocs.lat);
    localStorage.setItem('pogogyms_map_lng', mapLocs.lng);
    localStorage.setItem('pogogyms_map_zoom', mapLocs.zoom);
}

function getView() {
    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    var nw = new google.maps.LatLng(ne.lat(), sw.lng());
    var se = new google.maps.LatLng(sw.lat(), ne.lng());
    var p = [ne, nw, sw, se, ne];
    var mrk = new google.maps.Polygon({
        paths: p,
        strokeColor: 'red',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        fillColor: 'red',
        fillOpacity: 0.35
    });
    mrk.setMap(map);
}

function textos() {
    var txt = 'Listado de Nidos de Mendoza:';
    var fmt = 'Listado de Nidos de Mendoza:';
    var txtWO = '';
    var fmtWO = '';
    $.each(nestMarkers, function (i, item) {
        var n = item.nest;
        if (n.color == '#555555') {
            txt += '\n - ' + n.name + ': ' + n.pokemon.name;
            fmt += '\n - *' + n.name + ':* ' + n.pokemon.name;
        } else {
            txtWO += '\n - ' + n.name + ':';
            fmtWO += '\n - *' + n.name + ':*';
        }
    });
    if (txtWO.length > 0) {
        txt += txtWO;
    }
    if (fmtWO.length > 0) {
        fmt += fmtWO;
    }
    txt += '\n\nPara ver el mapa de nidos y gimnasios de Mendoza:\nhttps://pokemongomendoza.com/\nAhora puedes subir tu reporte de nido!\nComunidad Pokemon Go Mendoza';
    fmt += '\n\nPara ver el mapa de nidos y gimnasios de Mendoza:\nhttps://pokemongomendoza.com/\n_Ahora puedes subir tu reporte de nido!_\n_Comunidad Pokemon Go Mendoza_';
    $('#resumennidos_sinformato').val(txt);
    $('#resumennidos_conformato').val(fmt);
    $('#resumennidos_sinformato').click(function () {
        $('#resumennidos_sinformato').select();
        document.execCommand('copy');
        $('#copymsg').show();
        $('#copymsg_alert').text('Resumen de nidos sin formato copiado al portapapeles.');
    });
    $('#resumennidos_conformato').click(function () {
        $('#resumennidos_conformato').select();
        document.execCommand('copy');
        $('#copymsg').show();
        $('#copymsg_alert').text('Resumen de nidos con formato copiado al portapapeles.');
    });
}

function startFilters() {
    $('#filterGyms').prop('checked', filters.gyms == 'true');
    $('#filterExGyms').prop('checked', filters.exgyms == 'true');
    $('#filterNestMarker').prop('checked', filters.markers == 'true');
    $('#filterNestPolygons').prop('checked', filters.polygons == 'true');
    $('#filterPokestops').prop('checked', filters.pokestops == 'true');
    $('#filterS2Cell').prop('checked', filters.s2cells == 'true');
}

function switchGyms() {
    if (filters.gyms == 'true') {
        iterateFilter(markers, null);
        filters.gyms = 'false';
        $('#filterGyms').prop('checked', false);
    } else {
        iterateFilter(markers, map);
        filters.gyms = 'true';
        $('#filterGyms').prop('checked', true);
    }
}

function switchExGyms() {
    if (filters.exgyms == 'true') {
        iterateFilter(exmarkers, null);
        filters.exgyms = 'false';
        localStorage.setItem('pogogyms_filter_exgyms', 'false');
        $('#filterExGyms').prop('checked', false);
    } else {
        iterateFilter(exmarkers, map);
        filters.exgyms = 'true';
        localStorage.setItem('pogogyms_filter_exgyms', 'true');
        $('#filterExGyms').prop('checked', true);
    }
}

function switchPokestops() {
    if (filters.pokestops == 'true') {
        iterateFilter(pkStops, null);
        filters.pokestops = 'false';
        $('#filterPokestops').prop('checked', false);
    } else {
        iterateFilter(pkStops, map);
        filters.pokestops = 'true';
        $('#filterPokestops').prop('checked', true);
    }
}

function switchNestMarkers() {
    if (filters.markers == 'true') {
        iterateFilter(nestMarkersIcons, null);
        filters.markers = 'false';
        localStorage.setItem('pogogyms_filter_markers', 'false');
        $('#filterNestMarker').prop('checked', false);
    } else {
        iterateFilter(nestMarkersIcons, map);
        filters.markers = 'true';
        localStorage.setItem('pogogyms_filter_markers', 'true');
        $('#filterNestMarker').prop('checked', true);
    }
}

function switchNestPolygons() {
    if (filters.polygons == 'true') {
        iterateFilter(nestMarkers, null);
        filters.polygons = 'false';
        localStorage.setItem('pogogyms_filter_polygons', 'false');
        $('#filterNestPolygons').prop('checked', false);
    } else {
        iterateFilter(nestMarkers, map);
        filters.polygons = 'true';
        localStorage.setItem('pogogyms_filter_polygons', 'true');
        $('#filterNestPolygons').prop('checked', true);
    }
}

function switchS2Cells() {
    if (filters.s2cells == 'true') {
        iterateFilter(s2Cells, null);
        filters.s2cells = 'false';
        $('#filterS2Cell').prop('checked', false);
    } else {
        iterateFilter(s2Cells, map);
        filters.s2cells = 'true';
        $('#filterS2Cell').prop('checked', true);
    }
}

function iterateFilter(filter, map) {
    $.each(filter, function (i, item) {
        item.setMap(map);
    });
}

function listS2cells() {
    var list = {
        url: '/api/s2',
        body: null,
        type: 'GET',
        suc: iterateS2,
        msg: null
    };
    sendToServer(list);
}

function iterateS2(cells) {
    $.each(cells, function (i, item) {
        putCell(item);
    });
}

function putCell(cell) {
    var mrk = new google.maps.Polygon({
        paths: cell,
        strokeColor: '#00ff00',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        fillColor: '#00ff00',
        fillOpacity: 0.075
    });

    mrk.setMap(filters.s2cells == 'true' ? map : null);

    s2Cells[s2Cells.length] = mrk;
}


function listNests() {
    var list = {
        url: '/api/nests',
        body: null,
        type: 'GET',
        suc: iterateNests,
        msg: null
    };
    sendToServer(list);
}

function listGyms() {
    var list = {
        url: '/api/gyms',
        body: null,
        type: 'GET',
        suc: iterateGyms,
        msg: null
    };
    sendToServer(list);
}

function listPokestops() {
    var list = {
        url: '/api/pokestops',
        body: null,
        type: 'GET',
        suc: iteratePokestops,
        msg: null
    };
    sendToServer(list);
}

function getIcon(gym) {
    var color = '#777';
    var opaci = 2.0 / 3.0;
    var stk;
    if (gym.group != null) {
        stk = 1.0;
    } else {
        stk = 0.0;
    }
    if (gym.group != null) {
        var ex = gym.exPoints;
        if (ex < 1) {
            color = '#777';
        } else if (ex < 126) {
            color = '#d9534f';
        } else if (ex < 251) {
            color = '#f0ad4e';
        } else if (ex < 500) {
            color = '#5bc0de';
        } else {
            color = '#5cb85c';
        }
    }
    return {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: color,
        fillOpacity: opaci,
        scale: 7.5,
        strokeColor: '#000000',
        strokeOpacity: stk,
        strokeWeight: .75
    };
}

function iteratePokestops(pokestops) {
    $.each(pokestops, function (i, item) {
        putPokestop(item);
    });
}

function putPokestop(pokestop) {
    var opaci = 1.0 / 3.0;
    var location = new google.maps.LatLng(pokestop.lon, pokestop.lat);
    var marker = new google.maps.Marker({
        position: location,
        label: '',
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            fillColor: '#355a63',
            fillOpacity: opaci,
            scale: 5,
            strokeColor: '#006580',
            strokeOpacity: 0.5,
            strokeWeight: .5
        }
    });

    marker.setMap(filters.pokestops == 'true' ? map : null);

    pkStops[pkStops.length] = marker;
}

function putGym(gym) {
    var idx;
    if (gym.group != null) {
        idx = exmarkers.length;
    } else {
        idx = markers.length;
    }

    var location = new google.maps.LatLng(gym.lat, gym.lon);
    var marker = new google.maps.Marker({
        position: location,
        title: gym.name,
        label: {
            text: gym.group != null ? gym.group : ' ',
            fontSize: '11px'
        },
        icon: getIcon(gym),
        gym: gym
    });

    if (gym.group != null) {
        marker.setMap(filters.exgyms == 'true' ? map : null);
    } else {
        marker.setMap(filters.gyms == 'true' ? map : null);
    }


    marker.addListener('click', function () {
        clickGym(idx, gym.group != null);
    });


    if (gym.group != null) {
        exmarkers[exmarkers.length] = marker;
    } else {
        markers[markers.length] = marker;
    }
}

function iterateGyms(gyms) {
    $.each(gyms, function (i, item) {
        putGym(item);
    });
}

function putNest(nest) {
    var pointCount = 0;
    var maxLat = 0;
    var minLat = 0;
    var maxLon = 0;
    var minLon = 0;

    var idx = nestMarkers.length;

    var mrk = new google.maps.Polygon({
        nest: nest,
        paths: nest.polygon,
        strokeColor: nest.color,
        strokeOpacity: 1.0,
        strokeWeight: 2,
        fillColor: nest.color,
        fillOpacity: 0.35
    });

    for (var j = 0; j < nest.polygon.length; j++) {
        var ya = nest.polygon[j].lat;
        var yo = nest.polygon[j].lng;
        if (pointCount < 1) {
            minLat = maxLat = ya;
            minLon = maxLon = yo;
        } else {
            maxLat = ya > maxLat ? ya : maxLat;
            minLat = ya < minLat ? ya : minLat;
            maxLon = yo > maxLon ? yo : maxLon;
            minLon = yo < minLon ? yo : minLon;
        }
        pointCount++;
    };

    var pkmImg;

    if (nest.color == '#555555') {
        pkmImg = '/assets/sprites/' + nest.pokemon.number + '.png';
    } else {
        pkmImg = '/assets/sprites/0.png';
    }

    var pLat = (maxLat + minLat) / 2;
    var pLon = (maxLon + minLon) / 2;

    var markerImage = {
        url: pkmImg,
        scaledSize: new google.maps.Size(48, 48),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(24, 24)
    };

    mrk.setMap(filters.polygons == 'true' ? map : null);

    mrk.addListener('click', function () {
        clickNest(idx);
    });

    nestMarkers[idx] = mrk;

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(pLat, pLon),
        icon: markerImage
    });

    marker.addListener('click', function () {
        clickNest(idx);
    });

    marker.setMap(filters.markers == 'true' ? map : null);

    nestMarkersIcons[nestMarkersIcons.length] = marker;
}

function iterateNests(nests) {
    $.each(nests, function (i, item) {
        putNest(item);
    });
    textos();
}

function clickGym(idx, ex) {
    var g;
    if (ex) {
        g = exmarkers[idx].gym;
    } else {
        g = markers[idx].gym;
    }
    var id = g.gymId;
    var nombre = g.name != null ? g.name : 'Gimnasio sin nombre';
    var pic = g.picture != null ? g.picture : '/assets/nopic.jpg';
    $('#gyminfo_titulo').text(nombre);
    $('#gyminfo_pic').attr('src', pic);
    $('#gymId').val(id);
    $('#gymerr').hide();
    $('#gyminfofooter').show();
    if (g.group != null) {
        $('#reportraidcontainer').show();
        var ex = g.exPoints;
        var pr = g.exPoints >= 500 ? 100 : parseInt((ex * 100) / 500, 10);
        var pgr;
        if (ex < 126) {
            pgr = '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="' + pr + '" aria-valuemin="0" aria-valuemax="100" style="width:' + pr + '%">' + ex + '/500</div>';
        } else if (ex < 251) {
            pgr = '<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="' + pr + '" aria-valuemin="0" aria-valuemax="100" style="width:' + pr + '%">' + ex + '/500</div>';
        } else if (ex < 500) {
            pgr = '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="' + pr + '" aria-valuemin="0" aria-valuemax="100" style="width:' + pr + '%">' + ex + '/500</div>';
        } else {
            pgr = '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="' + pr + '" aria-valuemin="0" aria-valuemax="100" style="width:' + pr + '%">' + ex + '/500</div>';
        }
        $('#raidprogress').empty();
        $('#raidprogress').append(pgr);
    } else {
        $('#reportraidcontainer').hide();
        $('#reportraidcontainer').hide();
    }
    $('#gymUrl').attr('href', '/gym/' + id);
    $('#gyminfo').modal('show');
}

function newGym() {
    $('#inDir').val('');
    $('#inDet').val('');
    $('#nuevogym').modal('show');
    $('#alerterr').hide();
}

function clickNest(idx) {
    var nest = nestMarkers[idx].nest;
    var n = 0;
    var pic;
    if (nest.color == '#555555') {
        $('#nestnotupdated').hide();
    } else {
        $('#nestnotupdated').show();
    }
    if (nest.pokemon != null) {
        n = nest.pokemon.number;
        $('#nestnro').text(n + ' - ');
        $('#nestpkm').text(nest.pokemon.name);
        pic = '/assets/sprites/' + n + '.png';
    } else {
        $('#nestnro').text('');
        $('#nestpkm').text('Sin reporte');
        pic = '/assets/sprites/0.png';
    }
    $('#nestpkm_pic').attr('src', pic);
    if (nest.lastReport != null) {
        var d = new Date(nest.lastReport);
        $('#nestlast').text(formatDate(d));
    } else {
        $('#nestlast').text('sin fecha');
    }
    if (nest.user != null) {
        $('#nestuser').empty();
        $('#nestuser').append('<a href="/user/' + nest.userId + '" target="_blank">' + nest.user + '</a>');
    } else {
        $('#nestuser').text("Sin identificar");
    }
    var minsight;
    var maxsight;
    switch (nest.minSightings) {
        case 0:
            minsight = 'a veces ninguno';
            $('#minsightings').val('0');
            break;
        case 1:
            minsight = '1 a 3';
            $('#minsightings').val('1');
            break;
        case 2:
            minsight = '4 a 6';
            $('#minsightings').val('2');
            break;
        case 3:
            minsight = '7 a 9';
            $('#minsightings').val('3');
            break;
        case 4:
            minsight = 'más de 9';
            $('#minsightings').val('4');
            break;
        default:
            minsight = '???';
            $('#minsightings').val('-1');
            break;
    }
    switch (nest.maxSightings) {
        case 1:
            maxsight = 'al menos 1';
            $('#maxsightings').val('1');
            break;
        case 2:
            maxsight = '1 a 3';
            $('#maxsightings').val('2');
            break;
        case 3:
            maxsight = '4 a 6';
            $('#maxsightings').val('3');
            break;
        case 4:
            maxsight = '7 a 9';
            $('#maxsightings').val('4');
            break;
        case 5:
            maxsight = 'más de 9';
            $('#maxsightings').val('5');
            break;
        default:
            maxsight = '???';
            minsight = '???';
            $('#maxsightings').val('-1');
            $('#minsightings').val('-1');
            break;
    }
    $('#innestpokemon').val('' + n).trigger('change');
    $('#nestinfo_titulo').text('Nido de ' + nest.name);
    $('#minsightingsview').text(minsight);
    $('#maxsightingsview').text(maxsight);
    $('#nestId').val(nest.id);
    $('#nestUrl').attr('href', '/nest/' + nest.id);
    $('#nesterr').hide();
    $('#alert-report').hide();
    $('#nestrep').hide();
    $('#alert-report-button').attr('onclick', 'reportAbuse(' + idx + ');');
    $('#nestinfo').modal('show');
}

function getNestPokemons() {
    var get = {
        url: '/api/nest/pokemon',
        body: null,
        type: 'GET',
        suc: nestPkmOk,
        msg: null
    };
    sendToServer(get);
}

function nestPkmOk(ts) {
    $('#innestpokemon').empty().append('<option value="0">Sin reportar</option>');
    $.each(ts, function (i, item) {
        $('#innestpokemon').append('<option value="' + item.number + '">' + item.number + ' - ' + item.name + '</option>');
    });
    $('#innestpokemon').val('0');

    $("#innestpokemon").select2({
        theme: "bootstrap"
    });
}

function sendReport() {
    var minS = $('#minsightings').val();
    var maxS = $('#maxsightings').val();
    var report = {
        nestId: $('#nestId').val(),
        userId: $('#userId').val(),
        nestPokemon: parseInt($('#innestpokemon').val()),
        minSightings: minS == '-1' ? -1 : parseInt(minS),
        maxSightings: maxS == '-1' ? -1 : parseInt(maxS)
    };
    var post = {
        url: '/api/nest',
        body: JSON.stringify(report),
        type: 'POST',
        suc: succNestOk,
        msg: succNest
    }
    sendToServer(post);
}

function sendRaid() {
    var report = {
        gymId: $('#gymId').val(),
        userId: $('#userId').val(),
        level: parseInt($('#raidLevel').val()),
        players: parseInt($('#raidPlayers').val())
    };
    var post = {
        url: '/api/gym/raid',
        body: JSON.stringify(report),
        type: 'POST',
        suc: succRaidOk,
        msg: succRaid
    }
    sendToServer(post);
}

function reportAbuse(idx) {
    var nest = nestMarkers[idx].nest;
    if (nest.nestReportId == null) {
        succNest('No se puede denunciar un nido sin reporte previo');
        return;
    }
    if (nest.color != '#555555') {
        succNest('No se puede denunciar un nido que no ha sido actualizado tras la última migración de nidos');
        return;
    }
    var report = {
        nestId: nest.id,
        userId: $('#userId').val(),
        nestReportId: nest.nestReportId
    };
    var post = {
        url: '/api/nest/report',
        body: JSON.stringify(report),
        type: 'POST',
        suc: succNestReportOk,
        msg: succNest
    }
    sendToServer(post);
}

function succNestOk(data) {
    location.reload();
}

function succNest(msg) {
    $('#alert-report').hide();
    $('#nesterr').show();
    $('#alertnset').text(msg)
}

function succRaidOk(data) {
    location.reload();
}

function succRaid(msg) {
    $('#gymerr').show();
    $('#alertgym').text(msg)
}

function succNestReportOk(data) {
    $('#alert-report').hide();
    $('#nestrep').show();
    $('#nestrepmsg').text('Denuncia realizada correctamente');
}
