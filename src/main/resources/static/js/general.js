// var server = '/';
var server = '';

function sendToServer(req) {
    $.ajax({
        url: server + req.url,
        data: req.body,
        dataType: 'JSON',
        type: req.type,
        contentType: 'application/json',
        success: function (resp) {
            succFunc(req, resp);
        },
        error: function (error) {
            if (req.msg != null && error.responseJSON != null) {
                req.msg(error.responseJSON.message);
            } else {
                console.log('ERROR');
                console.log(error);
            }
        }
    });
}

function succFunc(req, resp) {
    if (resp.status == 'ok') {
        if (req.suc != null) {
            req.suc(resp.content);
        } else if (req.msg != null) {
            req.msg(resp.message);
        }
    } else {
        if (req.msg != null) {
            req.msg(resp.message);
        }
    }
}

function formatDate(date) {
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var monthNames = [
    "Enero", "Febrero", "Marzo",
    "Abril", "Mayo", "Junio", "Julio",
    "Agosto", "Septiembre", "Octubre",
    "Noviembre", "Diciembre"
  ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' de ' + monthNames[monthIndex] + ' del ' + year + ' a las ' + hour + ':' + minutes;
}
