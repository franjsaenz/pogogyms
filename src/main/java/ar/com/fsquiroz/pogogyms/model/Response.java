package ar.com.fsquiroz.pogogyms.model;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Response {

    private String status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object content;

    private Response() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public static Response ok(Object content) {
        return ok(null, content);
    }

    public static Response ok(String message) {
        return ok(message, null);
    }

    public static Response ok(String message, Object content) {
        Response r = new Response();
        r.status = "ok";
        r.message = message;
        r.content = content;
        return r;
    }

    public static Response error(Throwable throwable) {
        return error(throwable != null ? throwable.getMessage() : "NOT DEFINED");
    }

    public static Response error(String message) {
        Response r = new Response();
        r.status = "error";
        r.message = message;
        r.content = null;
        return r;
    }

}
