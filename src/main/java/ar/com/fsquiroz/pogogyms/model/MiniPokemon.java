package ar.com.fsquiroz.pogogyms.model;

import ar.com.fsquiroz.pogogyms.domain.Pokemon;

public class MiniPokemon {

    private int number;

    private String name;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static MiniPokemon build(Pokemon pokemon) {
        MiniPokemon p = new MiniPokemon();
        p.name = pokemon.getName();
        p.number = pokemon.getNumber();
        return p;
    }

}
