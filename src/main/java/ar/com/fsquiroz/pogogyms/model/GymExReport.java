package ar.com.fsquiroz.pogogyms.model;

public class GymExReport {

    private String gymId;

    private String userId;

    private boolean exCapable;

    public String getGymId() {
        return gymId;
    }

    public void setGymId(String gymId) {
        this.gymId = gymId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isExCapable() {
        return exCapable;
    }

    public void setExCapable(boolean exCapable) {
        this.exCapable = exCapable;
    }

}
