package ar.com.fsquiroz.pogogyms.model;

public class UserForm {

    private String userId;

    private String userNickname;

    private int userLevel;

    private String userTeam;

    private long userArrival;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserTeam() {
        return userTeam;
    }

    public void setUserTeam(String userTeam) {
        this.userTeam = userTeam;
    }

    public long getUserArrival() {
        return userArrival;
    }

    public void setUserArrival(long userArrival) {
        this.userArrival = userArrival;
    }

}
