package ar.com.fsquiroz.pogogyms.model;

import ar.com.fsquiroz.pogogyms.domain.Pokestop;
import java.util.LinkedList;
import java.util.List;

public class PokestopMarker {

    private String pokestopId;

    private double lat;

    private double lon;

    public PokestopMarker() {
    }

    public PokestopMarker(Pokestop p) {
        pokestopId = p.getId();
        lat = p.getLocation().getY();
        lon = p.getLocation().getX();
    }

    public String getPokestopId() {
        return pokestopId;
    }

    public void setPokestopId(String pokestopId) {
        this.pokestopId = pokestopId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public static List<PokestopMarker> build(List<Pokestop> pokestops) {
        List<PokestopMarker> markers = new LinkedList<>();
        for (Pokestop p : pokestops) {
            markers.add(new PokestopMarker(p));
        }
        return markers;
    }

}
