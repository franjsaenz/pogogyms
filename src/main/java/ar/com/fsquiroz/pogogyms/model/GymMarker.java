package ar.com.fsquiroz.pogogyms.model;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import java.util.ArrayList;
import java.util.List;

public class GymMarker {

    private String gymId;

    private String name;

    private String picture;

    private double lat;

    private double lon;

    private double distance;

    private int lv;

    private int exPoints;

    private boolean open;

    private String group;

    public String getGymId() {
        return gymId;
    }

    public void setGymId(String gymId) {
        this.gymId = gymId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public int getExPoints() {
        return exPoints;
    }

    public void setExPoints(int exPoints) {
        this.exPoints = exPoints;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

}
