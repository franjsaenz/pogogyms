package ar.com.fsquiroz.pogogyms.model;

import java.util.LinkedList;
import java.util.List;

public class NestMarker {

    private String id;

    private String name;

    private String user;

    private String userId;

    private String color;

    private Long lastReport;

    private String nestReportId;

    private MiniPokemon pokemon;

    private List<GoogleMapsPoint> polygon;

    private int minSightings;

    private int maxSightings;

    public NestMarker() {
        polygon = new LinkedList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getLastReport() {
        return lastReport;
    }

    public void setLastReport(Long lastReport) {
        this.lastReport = lastReport;
    }

    public String getNestReportId() {
        return nestReportId;
    }

    public void setNestReportId(String nestReportId) {
        this.nestReportId = nestReportId;
    }

    public MiniPokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(MiniPokemon pokemon) {
        this.pokemon = pokemon;
    }

    public List<GoogleMapsPoint> getPolygon() {
        return polygon;
    }

    public void setPolygon(List<GoogleMapsPoint> polygon) {
        this.polygon = polygon;
    }

    public int getMinSightings() {
        return minSightings;
    }

    public void setMinSightings(int minSightings) {
        this.minSightings = minSightings;
    }

    public int getMaxSightings() {
        return maxSightings;
    }

    public void setMaxSightings(int maxSightings) {
        this.maxSightings = maxSightings;
    }

}
