package ar.com.fsquiroz.pogogyms.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GymHunterGym {

    private Boolean enabled;

    private String gym_id;

    private String gym_name;

    private Integer gym_points;

    private Integer team_id;

    private String gym_inid;

    private Double longitude;

    private Double latitude;

    private List<Double> location;

    private String url;

    private String lastseen;

    public GymHunterGym() {
        location = new ArrayList<>();
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getGym_name() {
        return gym_name;
    }

    public void setGym_name(String gym_name) {
        this.gym_name = gym_name;
    }

    public Integer getGym_points() {
        return gym_points;
    }

    public void setGym_points(Integer gym_points) {
        this.gym_points = gym_points;
    }

    public Integer getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Integer team_id) {
        this.team_id = team_id;
    }

    public String getGym_inid() {
        return gym_inid;
    }

    public void setGym_inid(String gym_inid) {
        this.gym_inid = gym_inid;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLastseen() {
        return lastseen;
    }

    public void setLastseen(String lastseen) {
        this.lastseen = lastseen;
    }

}
