package ar.com.fsquiroz.pogogyms.model;

public class NestReport {

    private String nestId;

    private String userId;

    private String nestReportId;

    private int nestPokemon;

    private int minSightings;

    private int maxSightings;

    public String getNestId() {
        return nestId;
    }

    public void setNestId(String nestId) {
        this.nestId = nestId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNestReportId() {
        return nestReportId;
    }

    public void setNestReportId(String nestReportId) {
        this.nestReportId = nestReportId;
    }

    public int getNestPokemon() {
        return nestPokemon;
    }

    public void setNestPokemon(int nestPokemon) {
        this.nestPokemon = nestPokemon;
    }

    public int getMinSightings() {
        return minSightings;
    }

    public void setMinSightings(int minSightings) {
        this.minSightings = minSightings;
    }

    public int getMaxSightings() {
        return maxSightings;
    }

    public void setMaxSightings(int maxSightings) {
        this.maxSightings = maxSightings;
    }

}
