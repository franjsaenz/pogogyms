package ar.com.fsquiroz.pogogyms.model;

public class RaidForm {

    private String raidPokemon;

    private long raidTime;

    public String getRaidPokemon() {
        return raidPokemon;
    }

    public void setRaidPokemon(String raidPokemon) {
        this.raidPokemon = raidPokemon;
    }

    public long getRaidTime() {
        return raidTime;
    }

    public void setRaidTime(long raidTime) {
        this.raidTime = raidTime;
    }

}
