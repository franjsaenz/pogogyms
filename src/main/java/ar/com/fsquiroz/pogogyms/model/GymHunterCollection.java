package ar.com.fsquiroz.pogogyms.model;

import java.util.List;

public class GymHunterCollection {

    private List<String> gyms;

    private List<String> pokestops;

    private List<String> raids;

    public List<String> getGyms() {
        return gyms;
    }

    public void setGyms(List<String> gyms) {
        this.gyms = gyms;
    }

    public List<String> getPokestops() {
        return pokestops;
    }

    public void setPokestops(List<String> pokestops) {
        this.pokestops = pokestops;
    }

    public List<String> getRaids() {
        return raids;
    }

    public void setRaids(List<String> raids) {
        this.raids = raids;
    }

}
