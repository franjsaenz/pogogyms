package ar.com.fsquiroz.pogogyms.model;

import ar.com.fsquiroz.pogogyms.domain.ExInvitation;
import ar.com.fsquiroz.pogogyms.domain.NestMigration;
import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import ar.com.fsquiroz.pogogyms.domain.RaidPoint;
import ar.com.fsquiroz.pogogyms.domain.User;
import java.util.Date;

public class TimeLineItem implements Comparable<TimeLineItem> {

    private boolean report;

    private boolean exReport;

    private boolean nestMigration;

    private boolean exWave;

    private int raidLevel;

    private int raidPlayers;

    private int raidPoints;

    private String img;

    private String nestName;

    private String nestUrl;

    private String pokemon;

    private String userNickname;

    private String userUrl;

    private String gymName;

    private String gymUrl;

    private Date date;

    public TimeLineItem() {
    }

    public TimeLineItem(NestPokemonReport npr) {
        report = true;
        exReport = false;
        nestMigration = false;
        exWave = false;
        date = npr.getDate();
        nestName = npr.getNest().getName();
        nestUrl = "/nest/" + npr.getNest().getId();
        if (npr.getPokemon() == null) {
            img = "/assets/sprites/0.png";
            pokemon = "Sin identificar";
        } else {
            img = "/assets/sprites/" + npr.getPokemon().getNumber() + ".png";
            pokemon = npr.getPokemon().getName();
        }
        User user = npr.getUser();
        if (user != null) {
            userNickname = user.getNickname();
            userUrl = "/user/" + user.getId();
        }
    }

    public TimeLineItem(NestMigration nm) {
        report = false;
        exReport = false;
        nestMigration = true;
        exWave = false;
        date = nm.getDate();
    }

    public TimeLineItem(RaidPoint rp) {
        report = false;
        exReport = true;
        nestMigration = false;
        exWave = false;
        date = rp.getDate();
        raidLevel = rp.getLevel();
        raidPlayers = rp.getPlayers();
        raidPoints = rp.getPoints();
        User user = rp.getUser();
        if (user != null) {
            userNickname = user.getNickname();
            userUrl = "/user/" + user.getId();
        } else {
            userNickname = "usuario sin identificar";
            userUrl = "#";
        }
        gymName = rp.getGym().getName();
        gymUrl = "/gym/" + rp.getGym().getId();
    }

    public TimeLineItem(ExInvitation ei) {
        report = false;
        exReport = false;
        nestMigration = false;
        exWave = true;
        date = ei.getDate();
    }

    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }

    public boolean isExReport() {
        return exReport;
    }

    public void setExReport(boolean exReport) {
        this.exReport = exReport;
    }

    public boolean isNestMigration() {
        return nestMigration;
    }

    public void setNestMigration(boolean nestMigration) {
        this.nestMigration = nestMigration;
    }

    public boolean isExWave() {
        return exWave;
    }

    public void setExWave(boolean exWave) {
        this.exWave = exWave;
    }

    public int getRaidLevel() {
        return raidLevel;
    }

    public void setRaidLevel(int raidLevel) {
        this.raidLevel = raidLevel;
    }

    public int getRaidPlayers() {
        return raidPlayers;
    }

    public void setRaidPlayers(int raidPlayers) {
        this.raidPlayers = raidPlayers;
    }

    public int getRaidPoints() {
        return raidPoints;
    }

    public void setRaidPoints(int raidPoints) {
        this.raidPoints = raidPoints;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNestName() {
        return nestName;
    }

    public void setNestName(String nestName) {
        this.nestName = nestName;
    }

    public String getNestUrl() {
        return nestUrl;
    }

    public void setNestUrl(String nestUrl) {
        this.nestUrl = nestUrl;
    }

    public String getPokemon() {
        return pokemon;
    }

    public void setPokemon(String pokemon) {
        this.pokemon = pokemon;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public String getGymName() {
        return gymName;
    }

    public void setGymName(String gymName) {
        this.gymName = gymName;
    }

    public String getGymUrl() {
        return gymUrl;
    }

    public void setGymUrl(String gymUrl) {
        this.gymUrl = gymUrl;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(TimeLineItem o) {
        return o.getDate().compareTo(this.date);
    }

}
