package ar.com.fsquiroz.pogogyms.model;

import ar.com.fsquiroz.pogogyms.domain.Pokemon;
import ar.com.fsquiroz.pogogyms.domain.RaidTier;
import java.util.ArrayList;
import java.util.List;

public class RaidTierModel {

    private String id;

    private int level;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<RaidTierModel> build(RaidTier raidTier) {
        List<RaidTierModel> models = new ArrayList<>();
        RaidTierModel raid = new RaidTierModel();
        raid.id = raidTier.getLevel() + ".0";
        raid.level = raidTier.getLevel();
        raid.name = "Sin eclosionar";
        models.add(raid);
        for (Pokemon pokemon : raidTier.getPokemons()) {
            RaidTierModel r = new RaidTierModel();
            r.id = raidTier.getLevel() + "." + pokemon.getNumber();
            r.level = raidTier.getLevel();
            r.name = pokemon.getName();
            models.add(r);
        }
        return models;
    }

    public static List<RaidTierModel> build(List<RaidTier> raidTiers) {
        List<RaidTierModel> models = new ArrayList<>();
        for (RaidTier raidTier : raidTiers) {
            models.addAll(build(raidTier));
        }
        return models;
    }

}
