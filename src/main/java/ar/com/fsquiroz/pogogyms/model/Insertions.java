package ar.com.fsquiroz.pogogyms.model;

public class Insertions {

    private int pokestops;

    private int gyms;

    public int getPokestops() {
        return pokestops;
    }

    public void setPokestops(int pokestops) {
        this.pokestops = pokestops;
    }

    public void addPokestop() {
        pokestops++;
    }

    public int getGyms() {
        return gyms;
    }

    public void setGyms(int gyms) {
        this.gyms = gyms;
    }

    public void addGym() {
        gyms++;
    }

}
