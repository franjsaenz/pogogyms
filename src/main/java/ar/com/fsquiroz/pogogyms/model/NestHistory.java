package ar.com.fsquiroz.pogogyms.model;

import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class NestHistory {

    private String nestId;

    private String nestName;

    private String user;

    private Date date;

    private MiniPokemon pokemon;

    public NestHistory() {
    }

    public NestHistory(NestPokemonReport npr) {
        nestId = npr.getNest().getId();
        nestName = npr.getNest().getName();
        user = npr.getUser() != null ? npr.getUser().getNickname() : null;
        date = npr.getDate();
        pokemon = MiniPokemon.build(npr.getPokemon());
    }

    public String getNestId() {
        return nestId;
    }

    public void setNestId(String nestId) {
        this.nestId = nestId;
    }

    public String getNestName() {
        return nestName;
    }

    public void setNestName(String nestName) {
        this.nestName = nestName;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public MiniPokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(MiniPokemon pokemon) {
        this.pokemon = pokemon;
    }

    public static List<NestHistory> build(List<NestPokemonReport> npr) {
        List<NestHistory> histories = new LinkedList<>();
        for (NestPokemonReport n : npr) {
            NestHistory nh = new NestHistory(n);
            histories.add(nh);
        }
        return histories;
    }
}
