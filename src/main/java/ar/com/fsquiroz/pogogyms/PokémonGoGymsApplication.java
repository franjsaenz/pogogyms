package ar.com.fsquiroz.pogogyms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@PropertySources({
    @PropertySource(value = "classpath:application.properties")
    ,
    @PropertySource(value = "file:${user.home}${file.separator}.pogogyms${file.separator}application.properties", ignoreResourceNotFound = true)
})
public class PokémonGoGymsApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PokémonGoGymsApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PokémonGoGymsApplication.class);
    }

}
