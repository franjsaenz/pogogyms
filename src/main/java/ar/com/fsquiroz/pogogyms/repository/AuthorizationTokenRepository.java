package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.AuthorizationToken;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuthorizationTokenRepository extends MongoRepository<AuthorizationToken, String> {

}
