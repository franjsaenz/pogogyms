package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    public User findOneById(String id);

    public User findOneByNickname(String nickname);
    
    public User findOneByMail(String mail);

}
