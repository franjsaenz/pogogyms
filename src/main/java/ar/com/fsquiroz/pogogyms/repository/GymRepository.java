package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GymRepository extends MongoRepository<Gym, String> {

    public boolean existsByGymhuntrIdOrGymIdOrPhotoUrl(String gymhuntrId, String gymId, String photoUrl);

    public Gym findByGymhuntrIdOrGymIdOrPhotoUrl(String gymhuntrId, String gymId, String photoUrl);

    @Override
    public List<Gym> findAll(Sort sort);

    public List<Gym> findByExCapable(boolean exCapable);

    public List<Gym> findByVisibleIsTrue(Sort sort);

    public List<Gym> findByDateIsNull();

    public GeoResults<Gym> findByVisibleIsTrueAndLocationNear(Point point, Distance distance);

    public List<Gym> findByNameNull();

}
