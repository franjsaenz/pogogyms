package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.domain.RaidPoint;
import ar.com.fsquiroz.pogogyms.domain.User;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class RaidPointRepository {

    @Autowired
    private MongoOperations opps;

    public void save(RaidPoint rp) {
        opps.save(rp);
    }

    public List<RaidPoint> pointsByGym(Gym gym) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, -7);
        Query q = new Query();
        q.addCriteria(Criteria.where("gym").is(gym));
        q.addCriteria(Criteria.where("date").gt(c.getTime()));
        q.with(new Sort(Sort.Direction.DESC, "date"));
        return opps.find(q, RaidPoint.class);
    }

    public List<RaidPoint> pointsByUser(User user) {
        Query q = new Query();
        q.addCriteria(Criteria.where("user").is(user));
        q.with(new Sort(Sort.Direction.DESC, "date"));
        return opps.find(q, RaidPoint.class);
    }
    
    public List<RaidPoint> points() {
        return opps.findAll(RaidPoint.class);
    }
    
    public long countLastWaveReports(Date date) {
        Query q = new Query();
        q.addCriteria(Criteria.where("date").gt(date));
        return opps.count(q, RaidPoint.class);
    }

    public int exPoints(Gym gym) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, -7);
        List<RaidPoint> points = pointsByGym(gym);
        int p = 0;
        for (RaidPoint rp : points) {
            p += rp.getPoints();
        }
        return p;
    }

}
