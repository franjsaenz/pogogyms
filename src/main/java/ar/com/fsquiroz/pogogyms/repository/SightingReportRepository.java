package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Nest;
import ar.com.fsquiroz.pogogyms.domain.SightingReport;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SightingReportRepository extends MongoRepository<SightingReport, String> {

    public SightingReport findFirstByNestOrderByDateDesc(Nest nest);

}
