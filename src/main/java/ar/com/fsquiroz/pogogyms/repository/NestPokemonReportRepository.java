package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Nest;
import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import ar.com.fsquiroz.pogogyms.domain.User;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NestPokemonReportRepository extends MongoRepository<NestPokemonReport, String> {

    public long countByDateAfter(Date date);

    public NestPokemonReport findFirstByNestOrderByDateDesc(Nest nest);

    public List<NestPokemonReport> findByNestOrderByDateDesc(Nest nest);

    public List<NestPokemonReport> findByUserOrderByDateDesc(User user);

    public List<NestPokemonReport> findByOrderByDateDesc();

}
