package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.RaidTier;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RaidTierRepository extends MongoRepository<RaidTier, String> {

    public RaidTier findOneByLevel(int level);
    
}
