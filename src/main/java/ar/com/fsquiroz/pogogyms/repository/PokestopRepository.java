package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Pokestop;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PokestopRepository extends MongoRepository<Pokestop, String> {

    public boolean existsByGymhuntrId(String gymhuntrId);

}
