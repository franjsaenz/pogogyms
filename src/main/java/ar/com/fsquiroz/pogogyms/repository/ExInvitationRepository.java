package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.ExInvitation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ExInvitationRepository extends MongoRepository<ExInvitation, String> {

    public ExInvitation findFirstByRegularIsTrueOrderByDateDesc();

    public ExInvitation findFirstByOrderByDateDesc();

}
