package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.ContactMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ContactMessageRepository extends MongoRepository<ContactMessage, String> {

}
