package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Pokemon;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PokemonRepository extends MongoRepository<Pokemon, String> {

    public Pokemon findOneByNumber(int number);
    
    public Pokemon findOneByName(String name);
    
    public List<Pokemon> findByNumberBetween(int ini, int fin, Sort sort);
    
    @Override
    public List<Pokemon> findAll(Sort sort);
    
}
