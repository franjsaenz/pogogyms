package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.Nest;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NestRepository extends MongoRepository<Nest, String> {

    public List<Nest> findByPokemonNotNullAndLastUpdateNotNull();

    public List<Nest> findByCreatedNull();

}
