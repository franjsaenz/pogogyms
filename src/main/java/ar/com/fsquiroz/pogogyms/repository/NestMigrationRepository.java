package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.NestMigration;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NestMigrationRepository extends MongoRepository<NestMigration, String> {

    public NestMigration findFirstByOrderByDateDesc();

    public NestMigration findFirstByRegularIsTrueOrderByDateDesc();

    public List<NestMigration> findByOrderByDateDesc();

}
