package ar.com.fsquiroz.pogogyms.repository;

import ar.com.fsquiroz.pogogyms.domain.RestSesion;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RestSesionRepository extends MongoRepository<RestSesion, String> {

}
