package ar.com.fsquiroz.pogogyms.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class NestPokemonReport {

    @Id
    private String id;

    private Date date;

    @DBRef
    private Nest nest;

    @DBRef
    private NestMigration nestMigration;

    @DBRef
    private Pokemon pokemon;

    @DBRef
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Nest getNest() {
        return nest;
    }

    public void setNest(Nest nest) {
        this.nest = nest;
    }

    public NestMigration getNestMigration() {
        return nestMigration;
    }

    public void setNestMigration(NestMigration nestMigration) {
        this.nestMigration = nestMigration;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
