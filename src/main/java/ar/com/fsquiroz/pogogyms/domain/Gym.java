package ar.com.fsquiroz.pogogyms.domain;

import java.util.Date;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Gym {

    @Id
    private String id;

    private Date date;

    @Indexed
    private String name;

    private boolean visible;

    @Indexed(unique = true)
    private String gymhuntrId;

    private String gymId;

    private String photoUrl;

    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    private GeoJsonPoint location;

    private String exGroup;

    private boolean exCapable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getGymhuntrId() {
        return gymhuntrId;
    }

    public void setGymhuntrId(String gymhuntrId) {
        this.gymhuntrId = gymhuntrId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getGymId() {
        return gymId;
    }

    public void setGymId(String gymId) {
        this.gymId = gymId;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public GeoJsonPoint getLocation() {
        return location;
    }

    public void setLocation(GeoJsonPoint location) {
        this.location = location;
    }

    public String getExGroup() {
        return exGroup;
    }

    public void setExGroup(String exGroup) {
        this.exGroup = exGroup;
    }

    public boolean isExCapable() {
        return exCapable;
    }

    public void setExCapable(boolean exCapable) {
        this.exCapable = exCapable;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gym other = (Gym) obj;
        return Objects.equals(this.id, other.id);
    }

}
