package ar.com.fsquiroz.pogogyms.domain;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class SightingReport {

    @Id
    private String id;

    @DBRef
    private Nest nest;

    @DBRef
    private User user;

    private Date date;

    private int minSightings;

    private int maxSightings;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nest getNest() {
        return nest;
    }

    public void setNest(Nest nest) {
        this.nest = nest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMinSightings() {
        return minSightings;
    }

    public void setMinSightings(int minSightings) {
        this.minSightings = minSightings;
    }

    public int getMaxSightings() {
        return maxSightings;
    }

    public void setMaxSightings(int maxSightings) {
        this.maxSightings = maxSightings;
    }

}
