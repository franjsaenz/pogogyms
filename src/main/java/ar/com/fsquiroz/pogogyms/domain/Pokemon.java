package ar.com.fsquiroz.pogogyms.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Pokemon {

    @Id
    private String id;

    @Indexed(unique = true)
    private int number;

    private String name;

    @Indexed
    private boolean familyPrimary;

    @Indexed
    private String firstType;

    @Indexed
    private String secondType;

    private int atk;

    private int speAtk;

    private int def;

    private int speDef;

    private int sta;

    private int spe;

    @DBRef
    private Pokemon evolveFrom;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFamilyPrimary() {
        return familyPrimary;
    }

    public void setFamilyPrimary(boolean familyPrimary) {
        this.familyPrimary = familyPrimary;
    }

    public String getFirstType() {
        return firstType;
    }

    public void setFirstType(String firstType) {
        this.firstType = firstType;
    }

    public String getSecondType() {
        return secondType;
    }

    public void setSecondType(String secondType) {
        this.secondType = secondType;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getSpeAtk() {
        return speAtk;
    }

    public void setSpeAtk(int speAtk) {
        this.speAtk = speAtk;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public int getSpeDef() {
        return speDef;
    }

    public void setSpeDef(int speDef) {
        this.speDef = speDef;
    }

    public int getSta() {
        return sta;
    }

    public void setSta(int sta) {
        this.sta = sta;
    }

    public int getSpe() {
        return spe;
    }

    public void setSpe(int spe) {
        this.spe = spe;
    }

    public Pokemon getEvolveFrom() {
        return evolveFrom;
    }

    public void setEvolveFrom(Pokemon evolveFrom) {
        this.evolveFrom = evolveFrom;
    }

}
