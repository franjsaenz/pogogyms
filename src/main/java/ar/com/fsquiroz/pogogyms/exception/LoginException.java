package ar.com.fsquiroz.pogogyms.exception;

import org.springframework.http.HttpStatus;

public class LoginException extends PogoException {

    public LoginException(String nickname) {
        super("Contraseña incorrecta para " + nickname);
    }

    @Override
    public String getUserMessage() {
        return getMessage();
    }

    @Override
    public String getErrorMessage() {
        return getMessage();
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

}
