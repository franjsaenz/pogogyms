package ar.com.fsquiroz.pogogyms.exception;

import org.springframework.http.HttpStatus;

public abstract class PogoException extends Exception {

    protected PogoException(String message) {
        super(message);
    }

    protected PogoException(Throwable cause) {
        super(cause);
    }

    protected PogoException(String message, Throwable cause) {
        super(message, cause);
    }

    public abstract String getUserMessage();

    public abstract String getErrorMessage();

    public abstract HttpStatus getStatus();

}
