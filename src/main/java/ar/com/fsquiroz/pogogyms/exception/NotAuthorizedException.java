package ar.com.fsquiroz.pogogyms.exception;

import ar.com.fsquiroz.pogogyms.domain.User;
import org.springframework.http.HttpStatus;

public class NotAuthorizedException extends PogoException {

    private final String token;

    private final User user;

    public NotAuthorizedException(String message, User user) {
        super(message);
        this.user = user;
        token = null;
    }

    public NotAuthorizedException(String message, String token) {
        super(message);
        this.user = null;
        this.token = token;
    }

    @Override
    public String getUserMessage() {
        return super.getMessage();
    }

    @Override
    public String getErrorMessage() {
        if (user == null && token == null) {
            return getMessage();
        }
        StringBuilder sb = new StringBuilder(getMessage());
        if (user != null) {
            sb.append(" (User ");
            sb.append(user.getNickname());
            sb.append(")");
        } else if (token != null) {
            sb.append(" (Token ");
            sb.append(token);
            sb.append(")");
        }
        return sb.toString();
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.FORBIDDEN;
    }

}
