package ar.com.fsquiroz.pogogyms.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends PogoException {

    private String id;

    public NotFoundException(String id) {
        super("Resource not found. (id=" + id + ")");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getUserMessage() {
        return "No se puede encontrar el contenido";
    }

    @Override
    public String getErrorMessage() {
        return getMessage();
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }

}
