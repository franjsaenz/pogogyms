package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.domain.RestSesion;
import ar.com.fsquiroz.pogogyms.model.GymExReport;
import ar.com.fsquiroz.pogogyms.model.GymHunterCollection;
import ar.com.fsquiroz.pogogyms.model.Response;
import ar.com.fsquiroz.pogogyms.model.GymMarker;
import ar.com.fsquiroz.pogogyms.model.RaidReport;
import ar.com.fsquiroz.pogogyms.model.TimeReport;
import ar.com.fsquiroz.pogogyms.service.GymService;
import ar.com.fsquiroz.pogogyms.service.S2Service;
import com.google.common.geometry.S2Cell;
import com.google.gson.internal.LinkedHashTreeMap;
import java.util.List;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GymController {

    @Autowired
    private GymService gymService;

    @Autowired
    private S2Service s2Service;

    @GetMapping("/gyms")
    public Response getAll(@RequestParam(required = false) Double latitude,
            @RequestParam(required = false) Double longitude) throws Exception {
        List<GymMarker> gyms = gymService.listAll(latitude, longitude);
        return Response.ok(gyms);
    }

    @GetMapping("/gyms/geojson")
    public FeatureCollection getAll(@RequestHeader(required = false) String token) throws Exception {
        FeatureCollection fc;
        if (token != null) {
            RestSesion rs = gymService.getSesion(token);
            fc = gymService.gyms2Features(rs.getGyms());
        } else {
            fc = new FeatureCollection();
            List<GymMarker> gyms = gymService.listAll(null, null);
            for (GymMarker gym : gyms) {
                Feature f = new Feature();
                f.getProperties().put("name", gym.getName());
                Point p = new Point(gym.getLon(), gym.getLat());
                f.setGeometry(p);
                fc.add(f);
            }
        }
        return fc;
    }

    @GetMapping("/gyms/s2")
    public FeatureCollection gymsS2() {
        List<S2Cell> cells = s2Service.gyms();
        return s2Service.cell2Feature(cells);
    }

    @GetMapping("/gyms/sesion")
    public Response session() {
        RestSesion rs = gymService.create();
        return Response.ok("Sesion creada", rs.getId());
    }

    @PostMapping("/gyms")
    public ResponseEntity s2Center(@RequestHeader(required = true) String token,
            @RequestBody(required = true) GymHunterCollection collection) throws Exception {
        RestSesion rs = gymService.getSesion(token);
        int oldSize = rs.getGyms().size();
        gymService.gymhuntrGyms(rs, collection);
        int newSize = rs.getGyms().size();
        LinkedHashTreeMap<String, Integer> resp = new LinkedHashTreeMap<>();
        resp.put("Created", newSize - oldSize);
        resp.put("Before", oldSize);
        resp.put("Now", newSize);
        return new ResponseEntity(resp, HttpStatus.CREATED);
    }

    @GetMapping("/gyms/s2/center")
    public ResponseEntity s2Center(@RequestHeader(required = true) String token) throws Exception {
        RestSesion rs = gymService.getSesion(token);
        FeatureCollection fc = gymService.gym2S2Relative(rs.getGyms());
        return new ResponseEntity(fc, HttpStatus.OK);
    }

    @GetMapping("/gyms/s2/cells")
    public ResponseEntity s2Cells(@RequestHeader(required = true) String token) throws Exception {
        RestSesion rs = gymService.getSesion(token);
        List<S2Cell> cells = s2Service.gymsLv12(rs.getGyms());
        FeatureCollection fc = s2Service.cell2Feature(cells);
        return new ResponseEntity(fc, HttpStatus.OK);
    }

    @PostMapping("/gym/raid")
    public Response raid(@RequestBody RaidReport report) throws Exception {
        gymService.raidPoint(report);
        return Response.ok("Notificado");
    }

    @PostMapping("/gym/ex")
    public Response exGym(@RequestBody GymExReport gymExReport) throws Exception {
        Gym g = gymService.notifyExGym(gymExReport);
        return Response.ok("Notificado", gymService.build(g));
    }

    @PostMapping("/exinvitation")
    public Response exInvitation(@RequestBody TimeReport report) throws Exception {
        gymService.exReport(report);
        return Response.ok("ok");
    }

}
