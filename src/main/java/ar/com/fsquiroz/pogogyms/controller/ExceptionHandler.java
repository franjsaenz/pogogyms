package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.service.MailService;
import javax.servlet.http.HttpServletRequest;
import org.apache.catalina.connector.ClientAbortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice(annotations = Controller.class)
public class ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    @Autowired
    private MailService mailService;

    @org.springframework.web.bind.annotation.ExceptionHandler(ClientAbortException.class)
    public void clientAbortException(HttpServletRequest req, ClientAbortException ex) {
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public String error(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.info("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.info("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        mailService.exception(ex, req.getMethod(), req.getRequestURL().toString(), req.getQueryString());
        return "error/500";
    }

}
