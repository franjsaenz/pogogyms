package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.Response;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.connector.ClientAbortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice(annotations = RestController.class)
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(PogoException.class)
    public ResponseEntity<Response> pogo(HttpServletRequest req, PogoException pe) {
        log.debug(pe.getUserMessage(), pe);
        log.info("PogoException: {}", pe.getErrorMessage());
        logRequest(req);
        return new ResponseEntity(Response.error(pe.getUserMessage()), pe.getStatus());
    }

    @ExceptionHandler(ClientAbortException.class)
    public void exception(HttpServletRequest req, ClientAbortException ex) {
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> gral(HttpServletRequest req, Exception ex) {
        log.error(ex.getMessage(), ex);
        logRequest(req);
        return new ResponseEntity(Response.error("Hubo un error inesperado"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private void logRequest(HttpServletRequest req) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.info("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.info("[{}] {}", req.getMethod(), req.getRequestURL());
        }
    }

}
