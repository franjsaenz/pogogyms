package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.domain.Nest;
import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import ar.com.fsquiroz.pogogyms.domain.RaidPoint;
import ar.com.fsquiroz.pogogyms.domain.User;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.TimeLineItem;
import ar.com.fsquiroz.pogogyms.service.GymService;
import ar.com.fsquiroz.pogogyms.service.NestService;
import ar.com.fsquiroz.pogogyms.service.UserService;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ViewController {

    @Autowired
    private UserService userService;

    @Autowired
    private NestService nestService;

    @Autowired
    private GymService gymService;

    @GetMapping("/")
    public String home(HttpServletResponse response, Map<String, Object> model,
            @CookieValue(value = "userId", defaultValue = "") String userId,
            @RequestParam(required = false) String desarrollo) {
        if (userId != null && !userId.isEmpty()) {
            try {
                User u = userService.getById(userId);
                model.put("reporte", u.getBanned() == null);
                model.put("userId", u.getId());
                model.put("admin", u.isAdmin());
            } catch (PogoException po) {
                response.addCookie(new Cookie("userId", ""));
            }
        }
        model.put("desarrollo", desarrollo != null);
        return "mapa";
    }

    @GetMapping("/mapa")
    public String mapa(Map<String, Object> model,
            @RequestParam(required = false) String desarrollo) {
        return "redirect:/";
    }

    @GetMapping("/reportar")
    public String reportar(Map<String, Object> model,
            @RequestParam(required = false) String desarrollo) {
        return "redirect:/";
    }

    @GetMapping("/como")
    public String como() {
        return "como-cargar";
    }

    @GetMapping("/login")
    public String login(HttpServletResponse response,
            @CookieValue(value = "userId", defaultValue = "") String userId) {
        if (userId != null && !userId.isEmpty()) {
            try {
                userService.getById(userId);
                return "redirect:/";
            } catch (PogoException po) {
                response.addCookie(new Cookie("userId", ""));
            }
        }
        return "login";
    }

    @PostMapping("/login")
    public String login(HttpServletResponse response, Map<String, Object> model,
            @RequestParam String nickname,
            @RequestParam String password,
            @RequestParam(required = false) String remember) {
        try {
            User u = userService.login(nickname, password);
            Cookie c = new Cookie("userId", u.getId());
            if (!(remember == null || remember.isEmpty())) {
                c.setMaxAge(Integer.MAX_VALUE);
            }
            response.addCookie(c);
            return "redirect:/";
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
            return "login";
        }
    }

    @GetMapping("/signup")
    public String signup(HttpServletResponse response,
            @CookieValue(value = "userId", defaultValue = "") String userId) {
        if (userId != null && !userId.isEmpty()) {
            try {
                userService.getById(userId);
                return "redirect:/";
            } catch (PogoException po) {
                response.addCookie(new Cookie("userId", ""));
            }
        }
        return "signup";
    }

    @PostMapping("/signup")
    public String signup(Map<String, Object> model,
            @RequestParam String mail,
            @RequestParam String nickname,
            @RequestParam String password,
            @RequestParam String rePassword) {
        try {
            userService.create(mail, nickname, password, rePassword);
            model.put("success", "Cuenta creada exitosamente, hemos enviado un correo a su cuenta para confirmar el registro.");
            return "login";
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
            return "signup";
        }
    }

    @GetMapping("/logout")
    public String signout(HttpServletResponse response) {
        response.addCookie(new Cookie("userId", ""));
        return "redirect:/";
    }

    @GetMapping("/user/{userId}/activate")
    public String activar(Map<String, Object> model, @PathVariable String userId) {
        try {
            User u = userService.getById(userId);
            userService.activate(u);
            model.put("success", "Se ha activado exitosamente la cuenta. Ahora puede ingresar");
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
        }
        return "login";
    }

    @GetMapping("/user/reset")
    public String clean(Map<String, Object> model) {
        model.put("init", true);
        return "clean";
    }

    @PostMapping("/user/reset")
    public String clean(Map<String, Object> model,
            @RequestParam String mail) {
        try {
            User u = userService.getByMail(mail);
            userService.cleanPassword(u);
            model.put("success", true);
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
            model.put("init", true);
        }
        return "clean";
    }

    @GetMapping("/user/{userId}/reset")
    public String reset(Map<String, Object> model, @PathVariable String userId,
            @RequestParam(required = false) String token) {
        if (token != null) {
            model.put("init", true);
        } else {
            model.put("error", "Verifique haber escrito correctamente el link.");
        }
        return "reset";
    }

    @PostMapping("/user/{userId}/reset")
    public String reset(HttpServletResponse response, Map<String, Object> model,
            @PathVariable String userId,
            @RequestParam String token,
            @RequestParam String password,
            @RequestParam String rePassword) {
        try {
            User u = userService.getById(userId);
            userService.changePassword(u, token, password, rePassword);
            model.put("success", true);
            response.addCookie(new Cookie("userId", ""));
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
            model.put("init", true);
        }
        return "reset";
    }

    @GetMapping("/user/{profileId}")
    public String profile(HttpServletResponse response, Map<String, Object> model,
            @PathVariable String profileId, @CookieValue(value = "userId", defaultValue = "") String userId) {
        if (userId != null && !userId.isEmpty()) {
            try {
                User u = userService.getById(userId);
                model.put("admin", u.isAdmin());
            } catch (PogoException po) {
                response.addCookie(new Cookie("userId", ""));
            }
        }
        try {
            User u = userService.getById(profileId);
            List<NestPokemonReport> nprs = nestService.getNestHistory(u);
            List<RaidPoint> points = gymService.pointsTotal(u);
            List<TimeLineItem> items = nestService.getNestHistory(nprs);
            items.addAll(gymService.getPointsHistory(points));
            Collections.sort(items);
            model.put("items", items);
            model.put("reportSize", nprs.size() + points.size());
            model.put("user", u);
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
        }
        return "profile";
    }

    @GetMapping("/nest/{nestId}")
    public String nestProfile(Map<String, Object> model,
            @PathVariable String nestId) {
        try {
            Nest n = nestService.get(nestId);
            List<NestPokemonReport> nprs = nestService.getNestHistory(n);
            List<TimeLineItem> items = nestService.getNestHistory(nprs);
            model.put("items", items);
            model.put("reportSize", nprs.size());
            model.put("nest", n);
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
        }
        return "nest";
    }

    @GetMapping("/gym/{gymId}")
    public String gymHistory(HttpServletResponse response, Map<String, Object> model,
            @PathVariable String gymId, @CookieValue(value = "userId", defaultValue = "") String userId) {
        if (userId != null && !userId.isEmpty()) {
            try {
                User u = userService.getById(userId);
                model.put("admin", u.isAdmin());
                model.put("user", u);
            } catch (PogoException po) {
                response.addCookie(new Cookie("userId", ""));
            }
        }
        try {
            Gym g = gymService.get(gymId);
            List<RaidPoint> points = gymService.pointsTotal(g);
            List<TimeLineItem> items = gymService.getPointsHistory(points);
            int totalPoints = gymService.gymPoints(g);
            model.put("items", items);
            model.put("totalPoints", totalPoints);
            model.put("gym", g);
        } catch (PogoException pe) {
            model.put("error", pe.getUserMessage());
        } catch (Exception e) {
            model.put("error", "Ha habido un error imprevisto");
        }
        return "expoints";
    }

    @GetMapping("/history")
    public String nestsHistory(Map<String, Object> model) {
        List<NestPokemonReport> nprs = nestService.getNestHistory();
        int size = nprs.size();
        List<TimeLineItem> items = nestService.getNestHistory(nprs);
        try {
            List<RaidPoint> points = gymService.pointsTotal();
            items.addAll(gymService.getPointsHistory(points));
            Collections.sort(items);
            size += points.size();
            model.put("lastWaveReport", gymService.lastWaveReport());
        } catch (Exception e) {

        }
        model.put("items", items);
        model.put("reportSize", nprs.size());
        model.put("lastReportSize", nestService.countLastMigrationReports());
        return "history";
    }

}
