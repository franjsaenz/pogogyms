package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.model.Response;
import ar.com.fsquiroz.pogogyms.domain.Pokemon;
import ar.com.fsquiroz.pogogyms.repository.PokemonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PokemonController {

    @Autowired
    private PokemonRepository pokemonRepository;

    private Sort sort;

    public PokemonController() {
        sort = new Sort("number");
    }

    @GetMapping("/pokemon/{number}")
    public Response get(@PathVariable Integer number) throws Exception {
        Pokemon p = pokemonRepository.findOneByNumber(number);
        return Response.ok(p != null ? "Pokémon found" : "Pokémon not found", p);
    }

    @GetMapping("/pokemons")
    public Response getAll() throws Exception {
        List<Pokemon> ps = pokemonRepository.findAll(sort);
        return Response.ok(ps);
    }

}
