package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.model.Response;
import ar.com.fsquiroz.pogogyms.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SecurityController {

    @Autowired
    private SecurityService securityService;

    @PostMapping("/token")
    public Response createToken() throws Exception {
        securityService.create();
        return Response.ok("Token created");
    }

}
