package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.domain.Nest;
import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import ar.com.fsquiroz.pogogyms.domain.User;
import ar.com.fsquiroz.pogogyms.exception.NotAuthorizedException;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.MiniPokemon;
import ar.com.fsquiroz.pogogyms.model.NestHistory;
import ar.com.fsquiroz.pogogyms.model.NestMarker;
import ar.com.fsquiroz.pogogyms.model.NestReport;
import ar.com.fsquiroz.pogogyms.model.NewNest;
import ar.com.fsquiroz.pogogyms.model.Response;
import ar.com.fsquiroz.pogogyms.model.TimeReport;
import ar.com.fsquiroz.pogogyms.service.NestService;
import ar.com.fsquiroz.pogogyms.service.S2Service;
import ar.com.fsquiroz.pogogyms.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.geometry.S2Cell;

import java.util.List;

import org.apache.catalina.connector.ClientAbortException;
import org.geojson.FeatureCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class NestController {

    @Autowired
    private NestService nestService;

    @Autowired
    private UserService userService;

    @Autowired
    private S2Service s2Service;

    @Autowired
    private ObjectMapper mapper;

    @PostMapping("/polygons")
    public Response loadNest(@RequestBody NewNest newNest) throws Exception {
        User u = userService.getById(newNest.getUserId());
        if (!u.isAdmin()) {
            throw new NotAuthorizedException("Necesita permisos administrativos para esta acción", u);
        }
        nestService.loadNewNest(newNest, u);
        return Response.ok("ok");
    }

    @GetMapping("/s2")
    public Response listS2Cells(@RequestParam(required = false) Integer level) throws Exception {
        level = level != null ? level : S2Service.EX_GROUP_LEVEL;
        return Response.ok(s2Service.mendozaCellsAsGMap(level));
    }

    @GetMapping("/s2/geojson")
    public FeatureCollection listS2AsGeoJSON(@RequestParam(required = false) Integer level) throws Exception {
        level = level != null ? level : S2Service.EX_GROUP_LEVEL;
        return s2Service.mendozaCellsAsFeatureColection(level);
    }

    @GetMapping("/nests")
    public Response listNests() throws Exception {
        List<NestMarker> markers = nestService.getNests();
        return Response.ok(markers);

    }

    @PostMapping("/nest")
    public Response reportNest(@RequestBody(required = true) NestReport report) throws Exception {
        try {
            nestService.reportNest(report);
            return Response.ok("ok");
        } catch (PogoException | ClientAbortException e) {
            throw e;
        } catch (Exception e) {
            String json = mapper.writeValueAsString(report);
            System.out.println("Body: " + json);
            throw e;
        }
    }

    @GetMapping("/nest/{nestId}/s2")
    public FeatureCollection s2Nest(@PathVariable String nestId) throws Exception {
        Nest n = nestService.get(nestId);
        List<S2Cell> cells = s2Service.polygon2Cell(n.getArea());
        return s2Service.cell2Feature(cells);
    }

    @PostMapping("/nest/report")
    public Response reportAbuse(@RequestBody(required = true) NestReport report) throws Exception {
        nestService.reportAbuse(report);
        return Response.ok("ok");
    }

    @PostMapping("/nest/migration")
    public Response reportMigration(@RequestBody TimeReport report) throws Exception {
        nestService.reportMigratio(report);
        return Response.ok("ok");
    }

    @GetMapping("/nest/{nestId}/history")
    public Response nestHistory(@PathVariable String nestId) throws Exception {
        Nest n = nestService.get(nestId);
        List<NestPokemonReport> npr = nestService.getNestHistory(n);
        return Response.ok(NestHistory.build(npr));
    }

    @GetMapping("/nests/history")
    public Response nestHistory() throws Exception {
        List<NestPokemonReport> npr = nestService.getNestHistory();
        return Response.ok(NestHistory.build(npr));
    }

    @GetMapping("/nest/pokemon")
    public Response listPokemon() throws Exception {
        List<MiniPokemon> pokemon = nestService.nestPokemon();
        return Response.ok(pokemon);
    }

}
