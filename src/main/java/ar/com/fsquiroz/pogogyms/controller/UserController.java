package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.domain.User;
import ar.com.fsquiroz.pogogyms.exception.NotAuthorizedException;
import ar.com.fsquiroz.pogogyms.exception.NotValidException;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.ContactForm;
import ar.com.fsquiroz.pogogyms.model.Response;
import ar.com.fsquiroz.pogogyms.service.UserService;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/contact")
    public Response contact(@RequestBody(required = true) ContactForm contactForm) throws PogoException {
        userService.contactForm(contactForm);
        return Response.ok("Ok");
    }

    @DeleteMapping("/user/{profileId}")
    public Response deleteUser(HttpServletResponse response,
            @PathVariable String profileId,
            @CookieValue(value = "userId", defaultValue = "") String userId) throws PogoException {
        if (!(userId != null && !userId.isEmpty())) {
            throw new NotValidException("Insuficientes permisos.");
        }
        User u = userService.getById(userId);
        if (!u.isAdmin()) {
            throw new NotValidException("Insuficientes permisos.");
        }
        User p = userService.getById(profileId);
        userService.ban(p);
        return Response.ok("Usuario baneado correctamente");
    }

}
