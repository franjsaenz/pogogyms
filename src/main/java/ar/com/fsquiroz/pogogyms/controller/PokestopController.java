package ar.com.fsquiroz.pogogyms.controller;

import ar.com.fsquiroz.pogogyms.domain.Pokestop;
import ar.com.fsquiroz.pogogyms.model.GymHunterCollection;
import ar.com.fsquiroz.pogogyms.model.Insertions;
import ar.com.fsquiroz.pogogyms.model.PokestopMarker;
import ar.com.fsquiroz.pogogyms.model.Response;
import ar.com.fsquiroz.pogogyms.service.PokestopService;
import ar.com.fsquiroz.pogogyms.service.SecurityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PokestopController {

    @Autowired
    private PokestopService pokestopService;

    @Autowired
    private SecurityService securityService;

    @GetMapping("/pokestops")
    public Response pokestops() throws Exception {
        List<Pokestop> pokestops = pokestopService.listAll();
        return Response.ok(PokestopMarker.build(pokestops));
    }

    @PostMapping("/gymhuntr")
    public Response pokestops(@RequestHeader String auth,
            @RequestBody(required = true) GymHunterCollection collection) throws Exception {
        securityService.validate(auth);
        Insertions i = pokestopService.importGymHuntr(collection);
        return Response.ok(i);
    }

}
