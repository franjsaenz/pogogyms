package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.domain.Pokestop;
import ar.com.fsquiroz.pogogyms.model.GymHunterCollection;
import ar.com.fsquiroz.pogogyms.model.GymHunterGym;
import ar.com.fsquiroz.pogogyms.model.GymHunterPokestop;
import ar.com.fsquiroz.pogogyms.model.Insertions;
import ar.com.fsquiroz.pogogyms.repository.GymRepository;
import ar.com.fsquiroz.pogogyms.repository.PokestopRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

@Service
public class PokestopService {

    private final Logger log = LoggerFactory.getLogger(PokestopService.class);

    @Autowired
    private PokestopRepository pokestopRepository;

    @Autowired
    private GymRepository gymRepository;

    @Autowired
    private GymService gymService;

    @Autowired
    private ObjectMapper mapper;

    public Insertions importGymHuntr(GymHunterCollection collection) throws IOException {
        Insertions insertions = new Insertions();
        List<String> pokestops = collection.getPokestops();
        for (String p : pokestops) {
            GymHunterPokestop ghp = mapper.readValue(p, GymHunterPokestop.class);
            if (ghp.getPokestop_id().contains("=")) {
                continue;
            }
            if (!pokestopRepository.existsByGymhuntrId(ghp.getPokestop_id())) {
                log.info("Pokestop '{}'", ghp.getPokestop_id());
                Pokestop pokestop = new Pokestop();
                pokestop.setGymhuntrId(ghp.getPokestop_id());
                pokestop.setLocation(new GeoJsonPoint(ghp.getLongitude(), ghp.getLatitude()));
                pokestopRepository.insert(pokestop);
                insertions.addPokestop();
            }
        }
        List<String> gyms = collection.getGyms();
        for (String g : gyms) {
            GymHunterGym ghg = mapper.readValue(g, GymHunterGym.class);
            if (ghg.getGym_id().contains("=")) {
                continue;
            }
            Gym gym = gymRepository.findByGymhuntrIdOrGymIdOrPhotoUrl(ghg.getGym_inid(), ghg.getGym_id(), ghg.getUrl());
            if (gym == null) {
                log.info("Gym '{}' ({})", ghg.getGym_name(), ghg.getGym_inid());
                gym = gymService.gymhntr2Gym(ghg);
                gym.setVisible(true);
                gym.setGymId(ghg.getGym_id());
                gymRepository.insert(gym);
                insertions.addGym();
            } else {
                gym.setGymId(ghg.getGym_id());
                gym.setVisible(true);
                gymRepository.save(gym);
            }
        }
        return insertions;
    }

    public List<Pokestop> listAll() {
        return pokestopRepository.findAll();
    }

}
