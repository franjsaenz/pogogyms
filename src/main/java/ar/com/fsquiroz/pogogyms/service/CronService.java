package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.NestMigration;
import ar.com.fsquiroz.pogogyms.repository.NestMigrationRepository;

import java.lang.invoke.MethodHandles;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CronService {

    private final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private NestMigrationRepository nestMigrationRepository;

    @Scheduled(cron = "1 0 0 * * THU")
    public void migracion() {
        logger.info("Migración de Nidos");
        try {
            Date now = new Date();
            NestMigration last = nestMigrationRepository.findFirstByRegularIsTrueOrderByDateDesc();
            long diff = now.getTime() - last.getDate().getTime();
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            logger.info("{} días desde la última migración de nidos.", days);
            if (days >= 13) {
                logger.info("Registrando migración de Nidos");
                Instant i = Instant.now().truncatedTo(ChronoUnit.HOURS);
                NestMigration nm = new NestMigration();
                nm.setDate(Date.from(i));
                nm.setRegular(true);
                nestMigrationRepository.insert(nm);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

}
