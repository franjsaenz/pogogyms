package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.ContactMessage;
import ar.com.fsquiroz.pogogyms.domain.User;
import ar.com.fsquiroz.pogogyms.exception.LoginException;
import ar.com.fsquiroz.pogogyms.exception.NotFoundException;
import ar.com.fsquiroz.pogogyms.exception.NotValidException;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.ContactForm;
import ar.com.fsquiroz.pogogyms.repository.ContactMessageRepository;
import ar.com.fsquiroz.pogogyms.repository.UserRepository;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.bson.types.ObjectId;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactMessageRepository contactMessageRepository;

    @Autowired
    private MailService mailService;

    private StrongPasswordEncryptor spe;

    public UserService() {
        spe = new StrongPasswordEncryptor();
    }

    public User getById(String id) throws PogoException {
        User u = userRepository.findOneById(id);
        if (u == null) {
            throw new NotFoundException(id);
        }
        return u;
    }

    public User getByNickname(String nickname) throws PogoException {
        User u = userRepository.findOneByNickname(nickname);
        if (u == null) {
            throw new NotFoundException(nickname);
        }
        return u;
    }

    public User getByMail(String mail) throws PogoException {
        User u = userRepository.findOneByMail(mail);
        if (u == null) {
            throw new NotFoundException(mail);
        }
        return u;
    }

    public User login(String nickname, String password) throws PogoException {
        User u = userRepository.findOneByNickname(nickname);
        if (u == null) {
            throw new NotValidException("No se encuentra usuario con Nickname " + nickname);
        }
        if (u.getActivation() == null) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("nickname", nickname);
            throw new NotValidException("Todavía no activa la cuenta. Revise su correo. Si no ha llegado ningún correo, intente registrarse nuevamente.", params);
        }
        if (u.getBanned() != null) {
            throw new NotValidException("Su cuenta ha sido suspendida.");
        }
        if (!spe.checkPassword(password, u.getPassword())) {
            throw new LoginException(u.getNickname());
        }
        return u;
    }

    public void create(String mail, String nickname, String password, String rePassword) throws PogoException {
        User u = userRepository.findOneByMail(mail);
        if (u != null && u.getActivation() != null) {
            throw new NotValidException("Ya existe un usuario con mail " + mail);
        }
        if (u == null) {
            u = userRepository.findOneByNickname(nickname);
            if (u != null) {
                throw new NotValidException("Ya existe un usuario con nickname " + nickname);
            }
            if (!password.equals(rePassword)) {
                throw new NotValidException("Las contraseñas ingresadas no coinciden");
            }
            u = new User();
            u.setMail(mail);
            u.setNickname(nickname);
            u.setPassword(spe.encryptPassword(password));
            userRepository.insert(u);
        } else {
            u.setMail(mail);
            u.setNickname(nickname);
            u.setPassword(spe.encryptPassword(password));
            userRepository.save(u);
        }
        mailService.activar(u);
    }

    public void changePassword(User u, String token, String newPassword, String rePassword) throws PogoException {
        if (u.getTokenPassword() == null || !u.getTokenPassword().equals(token)) {
            throw new NotValidException("Código de seguridad incorrecto. Asegúrese de haber utilizado el link correctamente. Puede volver a pedir un reseteo de contraseña si persiste el error.");
        }
        if (!newPassword.equals(rePassword)) {
            throw new NotValidException("Las contraseñas ingresadas no coinciden");
        }
        u.setPassword(spe.encryptPassword(newPassword));
        u.setTokenPassword(null);
        userRepository.save(u);
    }

    public void cleanPassword(User u) throws PogoException {
        u.setTokenPassword(new ObjectId().toHexString());
        userRepository.save(u);
        mailService.reset(u);
    }

    public void activate(User u) throws PogoException {
        if (u.getActivation() != null) {
            throw new NotValidException("Esta cuenta ya se encontraba activada");
        }
        u.setActivation(new Date());
        userRepository.save(u);
    }

    public void contactForm(ContactForm contactForm) throws PogoException {
        ContactMessage message = new ContactMessage();
        if (contactForm.getUserId() != null) {
            User u = userRepository.findOneById(contactForm.getUserId());
            message.setUser(u);
        }
        message.setDate(new Date());
        message.setEmail(contactForm.getEmail());
        message.setMessage(contactForm.getMessage());
        message.setSubject(contactForm.getSubject());
        contactMessageRepository.insert(message);
        mailService.contactForm(message);
    }
    
    public void ban(User u) throws PogoException {
        if (u.getBanned() != null) {
            throw new NotValidException("Usuario previamente banneado.");
        }
        u.setBanned(new Date());
        userRepository.save(u);
    }

}
