package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.ExInvitation;
import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.domain.RaidPoint;
import ar.com.fsquiroz.pogogyms.domain.RestSesion;
import ar.com.fsquiroz.pogogyms.domain.User;
import ar.com.fsquiroz.pogogyms.exception.NotAuthorizedException;
import ar.com.fsquiroz.pogogyms.exception.NotFoundException;
import ar.com.fsquiroz.pogogyms.exception.NotValidException;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.GymExReport;
import ar.com.fsquiroz.pogogyms.model.GymHunterCollection;
import ar.com.fsquiroz.pogogyms.model.GymHunterGym;
import ar.com.fsquiroz.pogogyms.model.GymMarker;
import ar.com.fsquiroz.pogogyms.model.RaidReport;
import ar.com.fsquiroz.pogogyms.model.TimeLineItem;
import ar.com.fsquiroz.pogogyms.model.TimeReport;
import ar.com.fsquiroz.pogogyms.repository.ExInvitationRepository;
import ar.com.fsquiroz.pogogyms.repository.GymRepository;
import ar.com.fsquiroz.pogogyms.repository.RaidPointRepository;
import ar.com.fsquiroz.pogogyms.repository.RestSesionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

@Service
public class GymService {

    private final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private GymRepository gymRepository;

    @Autowired
    private RaidPointRepository raidPointRepository;

    @Autowired
    private ExInvitationRepository exInvitationRepository;

    @Autowired
    private RestSesionRepository restSesionRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private S2Service s2Service;

    @Autowired
    private ObjectMapper mapper;

    public List<GymMarker> listAll(Double latitude, Double longitude) throws Exception {
        List<GymMarker> markers;
        if (latitude != null && longitude != null) {
            markers = new LinkedList<>();
            GeoResults<Gym> results = gymRepository.findByVisibleIsTrueAndLocationNear(
                    new Point(longitude, latitude),
                    new Distance(60, Metrics.KILOMETERS)
            );
            for (GeoResult<Gym> result : results) {
                GymMarker gm = build(result.getContent());
                gm.setDistance(result.getDistance().in(Metrics.KILOMETERS).getValue());
                markers.add(gm);
            }
        } else {
            List<Gym> gyms = gymRepository.findByVisibleIsTrue(new Sort("name"));
            markers = build(gyms);
        }
        return markers;
    }

    public Gym get(String id) throws PogoException {
        Gym g = gymRepository.findOne(id);
        if (g == null) {
            throw new NotFoundException(id);
        }
        return g;
    }

    public RestSesion getSesion(String id) throws PogoException {
        RestSesion sesion = restSesionRepository.findOne(id);
        if (sesion == null) {
            throw new NotFoundException(id);
        }
        sesion.setLastAccess(new Date());
        restSesionRepository.save(sesion);
        return sesion;
    }

    public RestSesion create() {
        RestSesion rs = new RestSesion();
        rs.setDate(new Date());
        restSesionRepository.save(rs);
        return rs;
    }

    public void raidPoint(RaidReport report) throws Exception {
        Gym g = get(report.getGymId());
        User u = userService.getById(report.getUserId());
        if (u.getBanned() != null) {
            throw new NotAuthorizedException("Se ha restringido el reportes puntos EX a este usuario.", u);
        }
        RaidPoint rp = new RaidPoint();
        rp.setDate(new Date());
        rp.setGym(g);
        rp.setGroup(g.getExGroup());
        rp.setLevel(report.getLevel());
        rp.setPlayers(report.getPlayers());
        rp.setUser(u);
        rp.setPoints(report.getLevel() * report.getPlayers());
        raidPointRepository.save(rp);
    }

    public int gymPoints(Gym g) throws PogoException {
        return raidPointRepository.exPoints(g);
    }

    public List<RaidPoint> points(Gym g) throws PogoException {
        return raidPointRepository.pointsByGym(g);
    }

    public List<RaidPoint> pointsTotal(Gym g) throws PogoException {
        return raidPointRepository.pointsByGym(g);
    }

    public List<RaidPoint> pointsTotal(User u) throws PogoException {
        return raidPointRepository.pointsByUser(u);
    }

    public List<RaidPoint> pointsTotal() {
        return raidPointRepository.points();
    }

    public long lastWaveReport() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, -7);
        return raidPointRepository.countLastWaveReports(c.getTime());
    }

    public List<TimeLineItem> getPointsHistory(List<RaidPoint> points) throws PogoException {
        List<TimeLineItem> items = new LinkedList<>();
        List<ExInvitation> waves = exInvitationRepository.findAll(new Sort(Sort.Direction.DESC, "date"));
        ExInvitation next = next(waves);
        for (RaidPoint rp : points) {
            if (next != null && next.getDate().after(rp.getDate())) {
                TimeLineItem item = new TimeLineItem(next);
                items.add(item);
                next = next(waves);
            }
            TimeLineItem tli = new TimeLineItem(rp);
            items.add(tli);
        }
        while (next != null) {
            TimeLineItem item = new TimeLineItem(next);
            items.add(item);
            next = next(waves);
        }
        return items;
    }

    private ExInvitation next(List<ExInvitation> list) {
        if (list.isEmpty()) {
            return null;
        } else {
            ExInvitation ei = list.get(0);
            list.remove(0);
            return ei;
        }
    }

    public void exReport(TimeReport report) throws Exception {
        User u = userService.getById(report.getUserId());
        if (!u.isAdmin()) {
            throw new NotAuthorizedException("No posee privilegios para realizar esta acción", u);
        }
        ExInvitation ei = new ExInvitation();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, report.getHour());
        c.set(Calendar.MINUTE, report.getMinutes());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MINUTE, 180);
        if (new Date().before(c.getTime())) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("nickname", u.getNickname());
            params.put("reportDate", c.getTime().toString());
            throw new NotValidException("No se puede reportar un pase ex en el futuro", params);
        }
        ei.setDate(c.getTime());
        ei.setRegular(false);
        ei.setUser(u);
        ei.setCicle(0);
        exInvitationRepository.save(ei);
    }

    public void gymhuntrGyms(RestSesion sesion, GymHunterCollection collection) throws IOException {
        List<String> gyms = collection.getGyms();
        for (String g : gyms) {
            GymHunterGym ghg = mapper.readValue(g, GymHunterGym.class);
            Gym gym;
            if (!gymRepository.existsByGymhuntrIdOrGymIdOrPhotoUrl(ghg.getGym_inid(), ghg.getGym_id(), ghg.getUrl())) {
                gym = gymhntr2Gym(ghg);
                gymRepository.save(gym);
            } else {
                gym = gymRepository.findByGymhuntrIdOrGymIdOrPhotoUrl(ghg.getGym_inid(), ghg.getGym_id(), ghg.getUrl());
            }
            sesion.getGyms().add(gym);
        }
        restSesionRepository.save(sesion);
    }

    public FeatureCollection gym2S2Relative(Collection<Gym> gyms) {
        FeatureCollection fc = new FeatureCollection();
        for (Gym g : gyms) {
            fc.add(s2Service.gym2S2CenterPoint(g));
        }
        return fc;
    }

    public Gym gymhntr2Gym(GymHunterGym ghg) {
        Gym gym = new Gym();
        gym.setGymhuntrId(ghg.getGym_inid());
        gym.setName(ghg.getGym_name());
        gym.setPhotoUrl(ghg.getUrl());
        gym.setDate(new Date());
        gym.setLocation(new GeoJsonPoint(ghg.getLatitude(), ghg.getLongitude()));
        return gym;
    }

    public FeatureCollection gyms2Features(Collection<Gym> gyms) {
        FeatureCollection fc = new FeatureCollection();
        for (Gym g : gyms) {
            fc.add(gym2Feature(g));
        }
        return fc;
    }

    public Feature gym2Feature(Gym g) {
        Feature f = new Feature();
        f.setGeometry(new org.geojson.Point(g.getLocation().getX(), g.getLocation().getY()));
        f.setProperty("name", g.getName());
        f.setProperty("gymhuntrId", g.getGymhuntrId());
        return f;
    }

    public Gym notifyExGym(GymExReport report) throws Exception {
        User u = userService.getById(report.getUserId());
        if (!u.isAdmin()) {
            throw new NotAuthorizedException("No posee privilegios para realizar esta acción", u);
        }
        Gym g = get(report.getGymId());
        if (g.isExCapable() != report.isExCapable()) {
            g.setExGroup(null);
            g.setExCapable(report.isExCapable());
            gymRepository.save(g);
            updateGymGroups();
        }
        return get(report.getGymId());
    }

    public void updateGymGroups() {
        List<Gym> gyms = gymRepository.findByExCapable(true);
        if (gyms.isEmpty()) {
            return;
        }
        logger.info("Updating gyms' ex groups");
        s2Service.updateGymsGroup(gyms, S2Service.EX_GROUP_LEVEL);
        gymRepository.save(gyms);
        logger.info("{} gyms updated", gyms.size());
    }

    public GymMarker build(Gym gym) {
        GymMarker gm = new GymMarker();
        gm.setGymId(gym.getId());
        gm.setName(gym.getName());
        gm.setPicture(gym.getPhotoUrl());
        gm.setLon(gym.getLocation().getX());
        gm.setLat(gym.getLocation().getY());
        gm.setDistance(-1);
        gm.setLv(0);
        gm.setOpen(false);
        gm.setGroup(gym.getExGroup());
        if (gym.getExGroup() != null) {
            int points = raidPointRepository.exPoints(gym);
            gm.setExPoints(points);
        }
        return gm;
    }

    public List<GymMarker> build(List<Gym> gyms) throws Exception {
        List<GymMarker> markers = new ArrayList<>();
        for (Gym gym : gyms) {
            GymMarker m = build(gym);
            markers.add(m);
        }
        return markers;
    }

}
