package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.ContactMessage;
import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import ar.com.fsquiroz.pogogyms.domain.User;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailService {

    private final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private TemplateEngine template;

    private final String from = "pogomdza@gmail.com";

    public void activar(User u) {
        String url = "https://pokemongomendoza.com/user/" + u.getId() + "/activate";
        Context c = new Context();
        c.setVariable("url", url);
        c.setVariable("nickname", u.getNickname());
        String body = template.process("mailing/bienvenido", c);
        enviar(u.getMail(), null, "Bienvenid@ " + u.getNickname() + "!", body, true);
    }

    public void reset(User u) {
        String url = "https://pokemongomendoza.com/user/" + u.getId() + "/reset?token=" + u.getTokenPassword();
        Context c = new Context();
        c.setVariable("url", url);
        c.setVariable("nickname", u.getNickname());
        String body = template.process("mailing/password", c);
        enviar(u.getMail(), null, "Reseteo de contraseña", body, true);
    }

    public void report(User u, NestPokemonReport npr) {
        Context c = new Context();
        c.setVariable("u", u);
        c.setVariable("npr", npr);
        String body = template.process("mailing/denuncia", c);
        enviar(from, null, "Denuncia de usuario", body, true);
    }

    public void exception(Throwable t, String method, String url, String query) {
        String fullClass = t.getClass().getCanonicalName();
        String msg = t.getMessage();
        StringBuilder sb = new StringBuilder(SDF.format(new Date()));
        sb.append(" - Ocurrió un error en pokemongomendoza.com\n");
        sb.append("[");
        sb.append(method);
        sb.append("] ");
        sb.append(url);
        if (query != null && !query.isEmpty()) {
            sb.append("?");
            sb.append(query);
        }
        sb.append("\n");
        sb.append(fullClass);
        if (msg != null && !msg.isEmpty()) {
            sb.append(": ");
            sb.append(msg);
        }
        sb.append("\n");
        for (StackTraceElement ste : t.getStackTrace()) {
            sb.append("\tat ");
            sb.append(ste.getClassName());
            sb.append(".");
            sb.append(ste.getMethodName());
            sb.append("(");
            if (ste.isNativeMethod()) {
                sb.append("Método nativo");
            } else {
                sb.append(ste.getFileName());
                sb.append(":");
                sb.append(ste.getLineNumber());
            }
            sb.append(")\n");
        }
        enviar("franjsaenz@gmail.com", null, "Excepción sin controlar en pokemongomendoza.com", sb.toString(), false);
    }

    public void contactForm(ContactMessage contactMessage) {
        Context c = new Context();
        c.setVariable("c", contactMessage);
        String body = template.process("mailing/contact", c);
        enviar(from, contactMessage.getEmail(), contactMessage.getSubject(), body, true);
    }

    private void enviar(String email, String replyTo, String asunto, String contenido, boolean html) {
        _enviar(new String[]{email}, replyTo, asunto, contenido, html);
    }

    private void enviar(List<String> emails, String replyTo, String asunto, String contenido, boolean html) {
        _enviar(emails.toArray(new String[]{}), replyTo, asunto, contenido, html);
    }

    private void _enviar(String[] emails, String replyTo, String asunto, String contenido, boolean html) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                logger.info("Enviando a {} correo con asunto '{}'", emails, asunto);
                try {
                    MimeMessage message = sender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
                    helper.setFrom(from, "Comunidad Pokémon Go Mendoza");
                    helper.setTo(emails);
                    helper.setSubject(asunto);
                    helper.setText(contenido, html);
                    if (replyTo != null) {
                        helper.setReplyTo(replyTo);
                    }
                    sender.send(message);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }).start();
    }
}
