package ar.com.fsquiroz.pogogyms.service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class TimeHelper {

    public static final SimpleDateFormat STANDARD = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public static String format(Date date, Object ignored) {
        if (date == null) {
            return "sin fecha";
        }
        Instant when = date.toInstant().minus(3, ChronoUnit.HOURS);
        return STANDARD.format(Date.from(when));
    }

}
