package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.AuthorizationToken;
import ar.com.fsquiroz.pogogyms.exception.NotAuthorizedException;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.repository.AuthorizationTokenRepository;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {

    private final Logger log = LoggerFactory.getLogger(SecurityService.class);

    @Autowired
    private AuthorizationTokenRepository authorizationTokenRepository;

    public AuthorizationToken create() {
        AuthorizationToken at = new AuthorizationToken();
        at.setDate(new Date());
        authorizationTokenRepository.insert(at);
        log.info("New Authorization Token:\n{}", at.getId());
        return at;
    }

    public void validate(String id) throws PogoException {
        AuthorizationToken at = authorizationTokenRepository.findOne(id);
        if (at == null) {
            throw new NotAuthorizedException("Token inválido", id);
        }
        Date now = new Date();
        if ((at.getDate().getTime() + (1000 * 60 * 60 * 24)) < now.getTime()) {
            throw new NotAuthorizedException("Token expired", id);
        }
    }

}
