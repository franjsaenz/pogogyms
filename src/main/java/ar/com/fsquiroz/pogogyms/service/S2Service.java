package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.model.GoogleMapsPoint;
import ar.com.fsquiroz.pogogyms.repository.GymRepository;
import com.google.common.geometry.S2Cell;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2CellUnion;
import com.google.common.geometry.S2LatLng;
import com.google.common.geometry.S2Point;
import com.google.common.geometry.S2PolygonBuilder;
import com.google.common.geometry.S2Region;
import com.google.common.geometry.S2RegionCoverer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.LngLatAlt;
import org.geojson.Polygon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.stereotype.Service;

@Service
public class S2Service {

    public static final int EX_GROUP_LEVEL = 13;

    @Autowired
    private GymRepository gymRepository;

    public List<S2Cell> gyms() {
        List<Gym> gyms = gymRepository.findAll();
        Map<String, S2Cell> cells = new HashMap<>();
        for (Gym gym : gyms) {
            S2CellId id = gym2S2CellId(gym);
            cells.put(id.toToken(), new S2Cell(id));
        }
        return new ArrayList<>(cells.values());
    }

    public List<S2Cell> gymsLv12(Collection<Gym> gyms) {
        return gymsS2Cell(gyms, 12);
    }

    public List<S2Cell> gymsS2Cell(Collection<Gym> gyms, int level) {
        Map<String, S2Cell> cells = new TreeMap<>();
        for (Gym gym : gyms) {
            S2CellId id = gym2S2CellId(gym).parent(level);
            if (cells.containsKey(id.toToken())) {
                continue;
            }
            cells.put(id.toToken(), new S2Cell(id));
        }
        return new ArrayList<>(cells.values());
    }

    public void updateGymsGroup(Collection<Gym> gyms, int level) {
        Map<String, List<Gym>> group = new TreeMap<>();
        for (Gym gym : gyms) {
            S2CellId id = gym2S2CellId(gym).parent(level);
            addArrayToMap(group, id.toToken(), gym);
        }
        char g = 'A';
        for (String key : group.keySet()) {
            List<Gym> gs = group.get(key);
            for (Gym gym : gs) {
                gym.setExGroup(g + "");
            }
            g++;
        }
    }

    private <T> void addArrayToMap(Map<String, List<T>> map, String key, T value) {
        if (map.containsKey(key)) {
            map.get(key).add(value);
        } else {
            List<T> array = new ArrayList<>();
            array.add(value);
            map.put(key, array);
        }
    }

    public List<S2Cell> mendozaCells(int level) {
        List<Gym> gyms = gymRepository.findByExCapable(true);
        List<S2Cell> cells = gymsS2Cell(gyms, level);
        return cells;
    }

    public List<List<GoogleMapsPoint>> mendozaCellsAsGMap(int level) {
        List<List<GoogleMapsPoint>> polygons = new ArrayList<>();
        List<S2Cell> cells = mendozaCells(level);
        for (S2Cell cell : cells) {
            polygons.add(cell2GMap(cell));
        }
        return polygons;
    }

    public FeatureCollection mendozaCellsAsFeatureColection(int level) {
        FeatureCollection collection = new FeatureCollection();
        List<S2Cell> cells = mendozaCells(level);
        for (S2Cell cell : cells) {
            collection.add(cell2Feature(cell));
        }
        return collection;
    }

    public FeatureCollection cell2Feature(List<S2Cell> cells) {
        FeatureCollection collection = new FeatureCollection();
        for (S2Cell cell : cells) {
            collection.add(cell2Feature(cell));
        }
        return collection;
    }

    public List<S2Cell> cellId2Cell(List<S2CellId> cellIds) {
        List<S2Cell> cells = new ArrayList<>();
        for (S2CellId cellId : cellIds) {
            cells.add(new S2Cell(cellId));
        }
        return cells;
    }

    public S2CellId gym2S2CellId(Gym g) {
        S2LatLng ll = S2LatLng.fromDegrees(g.getLocation().getY(), g.getLocation().getX());
        S2CellId id = S2CellId.fromLatLng(ll);
        return id.parent(20);
    }

    public Feature gym2S2CenterPoint(Gym gym) {
        S2CellId id = gym2S2CellId(gym);
        S2Cell cell = new S2Cell(id);
        S2LatLng a = new S2LatLng(cell.getVertex(0));
        S2LatLng c = new S2LatLng(cell.getVertex(2));
        double lng = (a.lngDegrees() + c.lngDegrees()) / 2;
        double lat = (a.latDegrees() + c.latDegrees()) / 2;
        org.geojson.Point p = new org.geojson.Point(lng, lat);
        Feature f = new Feature();
        f.setProperty("name", gym.getName());
        f.setProperty("S2Lv20Token", id.toToken());
        f.setGeometry(p);
        return f;
    }

    public List<S2CellId> polygon2CellId(GeoJsonPolygon polygon) {
        S2Region region = polygon2Region(polygon);
        S2RegionCoverer coverer = new S2RegionCoverer();
        coverer.setMinLevel(0);
        coverer.setMaxLevel(20);
        coverer.setMaxCells(100);
        S2CellUnion union = coverer.getCovering(region);
        return union.cellIds();
    }

    public List<S2Cell> polygon2Cell(GeoJsonPolygon polygon) {
        List<S2CellId> ids = polygon2CellId(polygon);
        return cellId2Cell(ids);
    }

    public S2Region polygon2Region(GeoJsonPolygon polygon) {
        S2PolygonBuilder builder = new S2PolygonBuilder();
        List<Point> points = polygon.getPoints();
        for (int i = 0; i < points.size(); i++) {
            Point a = points.get(i);
            Point b = (i + 1) < points.size() ? points.get(i + 1) : points.get(0);
            S2Point pa = S2LatLng.fromDegrees(a.getY(), a.getX()).toPoint();
            S2Point pb = S2LatLng.fromDegrees(b.getY(), b.getX()).toPoint();
            builder.addEdge(pa, pb);
        }
        return builder.assemblePolygon();
    }

    public Polygon cell2Polygon(S2Cell cell) {
        S2LatLng a = new S2LatLng(cell.getVertex(0));
        S2LatLng b = new S2LatLng(cell.getVertex(1));
        S2LatLng c = new S2LatLng(cell.getVertex(2));
        S2LatLng d = new S2LatLng(cell.getVertex(3));
        List<LngLatAlt> points = new ArrayList<>();
        points.add(new LngLatAlt(a.lngDegrees(), a.latDegrees()));
        points.add(new LngLatAlt(b.lngDegrees(), b.latDegrees()));
        points.add(new LngLatAlt(c.lngDegrees(), c.latDegrees()));
        points.add(new LngLatAlt(d.lngDegrees(), d.latDegrees()));
        points.add(new LngLatAlt(a.lngDegrees(), a.latDegrees()));
        return new Polygon(points);
    }

    public Feature cell2Feature(S2Cell cell) {
        Feature f = new Feature();
        f.setProperty("token", cell.id().toToken());
        f.setProperty("level", cell.level());
        f.setGeometry(cell2Polygon(cell));
        f.setProperty("stroke", "#000000");
        f.setProperty("stroke-width", 2);
        f.setProperty("stroke-opacity", 1);
        f.setProperty("fill", "#0000ff");
        f.setProperty("fill-opacity", 0.2);
        return f;
    }

    public List<GoogleMapsPoint> cell2GMap(S2Cell cell) {
        List<GoogleMapsPoint> points = new ArrayList<>();
        S2LatLng a = new S2LatLng(cell.getVertex(0));
        S2LatLng b = new S2LatLng(cell.getVertex(1));
        S2LatLng c = new S2LatLng(cell.getVertex(2));
        S2LatLng d = new S2LatLng(cell.getVertex(3));
        points.add(new GoogleMapsPoint(a.latDegrees(), a.lngDegrees()));
        points.add(new GoogleMapsPoint(b.latDegrees(), b.lngDegrees()));
        points.add(new GoogleMapsPoint(c.latDegrees(), c.lngDegrees()));
        points.add(new GoogleMapsPoint(d.latDegrees(), d.lngDegrees()));
        points.add(new GoogleMapsPoint(a.latDegrees(), a.lngDegrees()));
        return points;
    }

}
