package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.Nest;
import ar.com.fsquiroz.pogogyms.domain.NestMigration;
import ar.com.fsquiroz.pogogyms.domain.NestPokemonReport;
import ar.com.fsquiroz.pogogyms.domain.Pokemon;
import ar.com.fsquiroz.pogogyms.domain.SightingReport;
import ar.com.fsquiroz.pogogyms.domain.User;
import ar.com.fsquiroz.pogogyms.exception.NotAuthorizedException;
import ar.com.fsquiroz.pogogyms.exception.NotFoundException;
import ar.com.fsquiroz.pogogyms.exception.NotValidException;
import ar.com.fsquiroz.pogogyms.exception.PogoException;
import ar.com.fsquiroz.pogogyms.model.GoogleMapsPoint;
import ar.com.fsquiroz.pogogyms.model.MiniPokemon;
import ar.com.fsquiroz.pogogyms.model.NestMarker;
import ar.com.fsquiroz.pogogyms.model.NestReport;
import ar.com.fsquiroz.pogogyms.model.NewNest;
import ar.com.fsquiroz.pogogyms.model.TimeLineItem;
import ar.com.fsquiroz.pogogyms.model.TimeReport;
import ar.com.fsquiroz.pogogyms.repository.NestMigrationRepository;
import ar.com.fsquiroz.pogogyms.repository.NestPokemonReportRepository;
import ar.com.fsquiroz.pogogyms.repository.NestRepository;
import ar.com.fsquiroz.pogogyms.repository.PokemonRepository;
import ar.com.fsquiroz.pogogyms.repository.SightingReportRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.GeoJsonObject;
import org.geojson.LngLatAlt;
import org.geojson.MultiPolygon;
import org.geojson.Polygon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPolygon;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class NestService {

    private final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private PokemonRepository pokemonRepository;

    @Autowired
    private NestRepository nestRepository;

    @Autowired
    private NestMigrationRepository nestMigrationRepository;

    @Autowired
    private NestPokemonReportRepository nestPokemonReportRepository;

    @Autowired
    private SightingReportRepository sightingReportRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @Autowired
    private ObjectMapper mapper;

    public Nest get(String nestId) throws PogoException {
        Nest n = nestRepository.findOne(nestId);
        if (n == null) {
            throw new NotFoundException(nestId);
        }
        return n;
    }

    public List<NestPokemonReport> getNestHistory(Nest n) {
        return nestPokemonReportRepository.findByNestOrderByDateDesc(n);
    }

    public List<NestPokemonReport> getNestHistory(User u) {
        return nestPokemonReportRepository.findByUserOrderByDateDesc(u);
    }

    public List<NestPokemonReport> getNestHistory() {
        return nestPokemonReportRepository.findByOrderByDateDesc();
    }

    public long countLastMigrationReports() {
        NestMigration nm = nestMigrationRepository.findFirstByOrderByDateDesc();
        return nestPokemonReportRepository.countByDateAfter(nm.getDate());
    }

    public List<TimeLineItem> getNestHistory(List<NestPokemonReport> nprs) {
        List<TimeLineItem> items = new LinkedList<>();
        List<NestMigration> migrations = nestMigrationRepository.findByOrderByDateDesc();
        NestMigration next = next(migrations);
        for (NestPokemonReport npr : nprs) {
            if (next != null && next.getDate().after(npr.getDate())) {
                TimeLineItem item = new TimeLineItem(next);
                items.add(item);
                next = next(migrations);
            }
            TimeLineItem tli = new TimeLineItem(npr);
            items.add(tli);
        }
        while (next != null) {
            TimeLineItem item = new TimeLineItem(next);
            items.add(item);
            next = next(migrations);
        }
        return items;
    }

    private NestMigration next(List<NestMigration> list) {
        if (list.isEmpty()) {
            return null;
        } else {
            NestMigration nm = list.get(0);
            list.remove(0);
            return nm;
        }
    }

    public List<NestMarker> getNests() throws Exception {
        NestMigration last = nestMigrationRepository.findFirstByOrderByDateDesc();
        List<Nest> nests = nestRepository.findAll(new Sort("name"));
        List<NestMarker> markers = new LinkedList<>();
        for (Nest nest : nests) {
            NestPokemonReport npr = nestPokemonReportRepository.findFirstByNestOrderByDateDesc(nest);
            NestMarker m = new NestMarker();
            m.setId(nest.getId());
            m.setName(nest.getName());
            Date d = last.getDate();
            if (npr != null) {
                MiniPokemon mp = npr.getPokemon() != null ? MiniPokemon.build(npr.getPokemon()) : null;
                m.setPokemon(mp);
                m.setLastReport(npr.getDate() != null ? npr.getDate().getTime() : null);
                m.setUser(npr.getUser() != null ? npr.getUser().getNickname() : null);
                m.setUserId(npr.getUser() != null ? npr.getUser().getId() : "no-user");
                d = npr.getDate() != null ? npr.getDate() : d;
                m.setNestReportId(npr.getId());
            }
            if (nest.getArea() != null) {
                m.setPolygon(parsePolygon(nest.getArea()));
            }
            if (m.getPokemon() == null) {
                m.setColor("#FF5722");
            } else if (d.after(last.getDate())) {
                m.setColor("#555555");
            } else {
                m.setColor("#FFB300");
            }
            SightingReport sr = sightingReportRepository.findFirstByNestOrderByDateDesc(nest);
            if (sr != null) {
                m.setMinSightings(sr.getMinSightings());
                m.setMaxSightings(sr.getMaxSightings());
            } else {
                m.setMinSightings(-1);
                m.setMaxSightings(-1);
            }
            markers.add(m);
        }
        return markers;
    }

    private List<GoogleMapsPoint> parsePolygon(GeoJsonPolygon polygon) {
        if (polygon == null) {
            return null;
        }
        List<GoogleMapsPoint> points = new LinkedList<>();
        for (Point point : polygon.getPoints()) {
            GoogleMapsPoint p = new GoogleMapsPoint();
            p.setLat(point.getY());
            p.setLng(point.getX());
            points.add(p);
        }
        return points;
    }

    public List<MiniPokemon> nestPokemon() {
        List<Pokemon> pokemon = pokemonRepository.findByNumberBetween(0, 650, new Sort("number"));
        List<MiniPokemon> minis = new LinkedList<>();
        for (Pokemon p : pokemon) {
            minis.add(MiniPokemon.build(p));
        }
        return minis;
    }

    public void reportNest(NestReport report) throws Exception {
        User u = userService.getById(report.getUserId());
        if (u.getBanned() != null) {
            throw new NotAuthorizedException("Se ha restringido el reportes de nido a este usuario.", u);
        }
        Nest n = nestRepository.findOne(report.getNestId());
        if (n == null) {
            throw new NotFoundException(report.getNestId());
        }
        if (report.getMaxSightings() < report.getMinSightings()) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("nickname", u.getNickname());
            params.put("nestId", n.getId());
            params.put("nestName", n.getName());
            params.put("max", report.getMaxSightings() + "");
            params.put("min", report.getMinSightings() + "");
            throw new NotValidException("La cantidad de avistamientos mínimos por hora no puede ser mayor a los máximos por hora", params);
        }
        boolean sightingsChange = false;
        SightingReport sr = sightingReportRepository.findFirstByNestOrderByDateDesc(n);
        if (sr != null
                && (sr.getMaxSightings() != report.getMaxSightings()
                || sr.getMinSightings() != report.getMinSightings())) {
            sightingsChange = true;
        }
        sightingsChange = sightingsChange || sr == null;
        sightingsChange = sightingsChange && report.getMaxSightings() > -1 && report.getMinSightings() > -1;
        if (sightingsChange) {
            sr = new SightingReport();
            sr.setDate(new Date());
            sr.setMaxSightings(report.getMaxSightings());
            sr.setMinSightings(report.getMinSightings());
            sr.setNest(n);
            sr.setUser(u);
            sightingReportRepository.insert(sr);
        }
        Pokemon p = pokemonRepository.findOneByNumber(report.getNestPokemon());
        if (p == null) {
            throw new NotFoundException(report.getNestPokemon() + "");
        }
        NestMigration nm = nestMigrationRepository.findFirstByOrderByDateDesc();
        NestPokemonReport npr = nestPokemonReportRepository.findFirstByNestOrderByDateDesc(n);
        if (npr != null
                && npr.getNestMigration() != null
                && npr.getNestMigration().equals(nm)
                && npr.getPokemon() != null
                && npr.getPokemon().getNumber() == p.getNumber()) {
            if (sightingsChange) {
                return;
            }
            Map<String, String> params = new LinkedHashMap<>();
            params.put("nickname", u.getNickname());
            params.put("nestId", n.getId());
            params.put("nestName", n.getName());
            params.put("migrationDate", npr.getNestMigration().getDate().toString());
            params.put("oldDate", npr.getDate().toString());
            params.put("oldPkm", npr.getPokemon().getName());
            params.put("newPkm", p.getName());
            throw new NotValidException("Este Pókemon ya se encuentra reportado en este nido en esta migración", params);
        }
        npr = new NestPokemonReport();
        npr.setDate(new Date());
        npr.setNest(n);
        npr.setNestMigration(nm);
        npr.setPokemon(p);
        npr.setUser(u);
        nestPokemonReportRepository.insert(npr);
    }

    public void reportAbuse(NestReport report) throws Exception {
        NestPokemonReport npr = nestPokemonReportRepository.findOne(report.getNestReportId());
        if (npr == null) {
            throw new NotFoundException(report.getNestReportId());
        }
        User u = userService.getById(report.getUserId());
        mailService.report(u, npr);
    }

    public void reportMigratio(TimeReport report) throws Exception {
        User u = userService.getById(report.getUserId());
        if (!u.isAdmin()) {
            throw new NotAuthorizedException("No posee privilegios para realizar esta acción", u);
        }
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, report.getHour());
        c.set(Calendar.MINUTE, report.getMinutes());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.MINUTE, 180);
        if (new Date().before(c.getTime())) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put("nickname", u.getNickname());
            params.put("reportDate", c.getTime().toString());
            throw new NotValidException("No se puede reportar una migración en el futuro", params);
        }
        NestMigration nm = new NestMigration();
        nm.setDate(c.getTime());
        nm.setRegular(false);
        nm.setUser(u);
        nestMigrationRepository.insert(nm);
    }

    public Nest loadNewNest(NewNest nn, User user) throws PogoException {
        FeatureCollection features;
        try {
            features = mapper.readValue(nn.getGeojson(), FeatureCollection.class);
        } catch (IOException e) {
            throw new NotValidException("Hubo un error al procesar el GeoJSON subido.");
        }
        if (features.getFeatures().isEmpty()) {
            throw new NotValidException("No se encontraron nidos en el GeoJSON subido.");
        } else if (features.getFeatures().size() > 1) {
            throw new NotValidException("Se encontraron múltiples nidos en el GeoJSON subido. Se permite uno solo por GeoJSON.");
        }
        for (Feature f : features) {
            GeoJsonObject object = f.getGeometry();
            GeoJsonPolygon polygon;
            if (object instanceof MultiPolygon) {
                MultiPolygon mp = (MultiPolygon) object;
                System.out.print("\tMultoPolygon");
                polygon = parser(mp);
                System.out.println();
            } else if (object instanceof Polygon) {
                Polygon p = (Polygon) object;
                System.out.println("\tPolygon");
                polygon = parser(p);
            } else {
                throw new NotValidException("No se encontró un polígono válido para el nido.");
            }
            log.info("{} registered nest {}", user.getNickname(), nn.getName());
            Nest n = new Nest();
            n.setArea(polygon);
            n.setName(nn.getName());
            n.setCreated(new Date());
            return nestRepository.insert(n);
        }
        throw new NotValidException("Hubo un error inesperado y no se pudo guardar el nido.");
    }

    private GeoJsonPolygon parser(Polygon p) {
        List<List<LngLatAlt>> ps = p.getCoordinates();
        for (List<LngLatAlt> _p : ps) {
            List<Point> points = new LinkedList<>();
            for (LngLatAlt l : _p) {
                Point point = new Point(l.getLongitude(), l.getLatitude());
                points.add(point);
            }
            return new GeoJsonPolygon(points);
        }
        return null;
    }

    private GeoJsonPolygon parser(MultiPolygon mp) {
        List<GeoJsonPolygon> polygons = new LinkedList<>();
        for (List<List<LngLatAlt>> ps : mp.getCoordinates()) {
            for (List<LngLatAlt> _p : ps) {
                List<Point> points = new LinkedList<>();
                for (LngLatAlt l : _p) {
                    Point point = new Point(l.getLongitude(), l.getLatitude());
                    points.add(point);
                }
                GeoJsonPolygon polygon = new GeoJsonPolygon(points);
                polygons.add(polygon);
                System.out.print("+");
            }
        }
        return polygons.isEmpty() ? null : polygons.get(0);
    }

}
