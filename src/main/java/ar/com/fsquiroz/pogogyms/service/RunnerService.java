package ar.com.fsquiroz.pogogyms.service;

import ar.com.fsquiroz.pogogyms.domain.Gym;
import ar.com.fsquiroz.pogogyms.domain.Nest;
import ar.com.fsquiroz.pogogyms.repository.GymRepository;
import ar.com.fsquiroz.pogogyms.repository.NestRepository;
import java.lang.invoke.MethodHandles;
import java.util.List;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

@Service
public class RunnerService implements ApplicationRunner {

    private final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private NestRepository nestRepository;

    @Autowired
    private GymRepository gymRepository;

    @Autowired
    private GymService gymService;

    @Autowired
    private S2Service s2Service;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        try {
            updateNest();
            updateGyms();
            updateGymGroups();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void updateNest() throws Exception {
        List<Nest> nests = nestRepository.findByCreatedNull();
        if (nests.isEmpty()) {
            return;
        }
        logger.info("Updating nest entities");
        for (Nest nest : nests) {
            ObjectId id = new ObjectId(nest.getId());
            nest.setCreated(id.getDate());
            nestRepository.save(nest);
        }
        logger.info("{} nest entities updated", nests.size());
    }

    private void updateGyms() {
        List<Gym> gyms = gymRepository.findByDateIsNull();
        if (gyms.isEmpty()) {
            return;
        }
        logger.info("Updating gyms entities");
        for (Gym g : gyms) {
            ObjectId id = new ObjectId(g.getId());
            g.setDate(id.getDate());
            gymRepository.save(g);
        }
        logger.info("{} gyms entities updated", gyms.size());
    }

    private void updateGymGroups() {
        gymService.updateGymGroups();
    }

}
